from setuptools import *

setup(
    name='task_tracker',
    version='0.0.0.1',
    packages=['lib', 'lib.tests', 'app'],
    url='',
    license='',
    author='liam',
    entry_points={
        'console_scripts':
            [
                'task=app.parser:main'
            ]
    },
    author_email='',
    description='',
    install_requires='peewee==3.3.4'
)