import sys
import re
import os
import datetime
import logging
import unittest
import json
import argparse
import lib.peewee_models
import lib.logging
from lib.user import User
from lib.plan import Plan
from lib.issue import Issue, DeadlineActions, TaskStatus
from lib.issue_base import PermissionType
from lib.schedule import Schedule, PeriodType
from lib.db_interface import connect, close, change_database
from lib.tests import test_suite

HOME = os.getenv('HOME')
APP_NAME = 'TaskTracker'
MISSING_ARGS_ERR_MSG = 'Some parameters are missing'
DEFAULT_CONFIG_PATH = os.path.join(HOME, APP_NAME, 'config.txt')


class AuthenticationError(Exception):
    def __init__(self, msg):
        super().__init__(self, msg)


class Actions:

    @staticmethod
    def create_user(args):
        if args.login is None or args.password is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        User.create(args.login, args.password)

    @staticmethod
    def log_in(args):
        with open(args.config) as input:
            config = json.load(input)
        if args.login is None or args.password is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        config['login'] = args.login
        config['password'] = args.password
        with open(args.config, 'w') as out:
            json.dump(config, out)
        user = User.log_in(config['login'], config['password'])
        if user is None:
            raise AuthenticationError('Incorrect login or password')

    @staticmethod
    def get_user_from_file(args):
        with open(args.config) as input:
            config = json.load(input)
        user = User.log_in(config['login'], config['password'])
        if user is None:
            raise AuthenticationError('Incorrect login or password')
        return user

    @staticmethod
    def change_password(args):
        if args.password is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        user = Actions.get_user_from_file(args)
        user.password = args.password
        user.save()

    @staticmethod
    def change_login(args):
        if args.login is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        user = Actions.get_user_from_file(args)
        user.login = args.login
        user.save()

    @staticmethod
    def show_users(args):
        for login in User.all_logins():
            print(login)

    @staticmethod
    def issues_list(args):
        connect()
        user = Actions.get_user_from_file(args)
        issues = args.issue_class.get_visible_to(user)
        if hasattr(args, 'status') and args.status is not None:
            issues = [issue for issue in issues if issue.get_status(user) == args.status]
        close()
        if args.title is not None:
            issues = [issue for issue in issues if re.match(args.title, issue.title, re.I)]
        if args.description is not None:
            issues = [issue for issue in issues if re.match(args.description, issue.description, re.I)]
        if args.priority is not None:
            issues = [issue for issue in issues if issue.priority > args.priority]
        if args.tag is not None:
            issues = [issue for issue in issues if args.tag in issue.tags(user)]
        for issue in range(0, len(issues)):
            print(str(issues[issue].id) + ' - ' + issues[issue].title)

    @staticmethod
    def show_issue(args):
        connect()
        user = Actions.get_user_from_file(args)
        if args.id is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        issue = args.issue_class.find_by_id(args.id)
        if issue is None:
            print('None')
            close()
            return
        if not issue.is_visible_to(user):
            print('You cannot watch this issue')
            close()
            return

        print('Title: ' + issue.title)
        print('Status: ' + str(TaskStatus(issue.get_status(user)).name))
        tags = issue.tags(user)
        if tags:
            print('Tags:')
            for tag in tags:
                print(tag)
        print('Description: ' + issue.description)
        print('Priority: ' + str(issue.priority))
        if issue.deadline is not None:
            print('Deadline: ' + (issue.deadline.isoformat(' ')))
            print('Deadline action: ' + str(DeadlineActions(issue.deadline_action).name))
        print('Created: ' + (issue.creation_time.isoformat(' ')))
        print('Owner: ' + issue.owner.login)

        if issue.is_managed_by(user):
            if issue.parent_issue is None:
                print('Parent: None')
            else:
                print('Parent: ' + str(issue.parent_issue.id) + ' - ' + issue.parent_issue.title)
            print('Children: ')
            for issue in issue.children_issues:
                print(str(issue.id) + ' - ' + issue.title)
            print('Alternatives: ')
            for issue in issue.alternatives:
                print(str(issue.id) + ' - ' + issue.title)
            print('Blocked by: ')
            for issue in issue.blocked_by:
                print(str(issue.id) + ' - ' + issue.title)
            print('Mutually exclusive: ')
            for issue in issue.mutually_exclusives:
                print(str(issue.id) + ' - ' + issue.title)
            if issue.is_administrated_by(user):
                print('Permissions: ')
                for permission in issue.all_permissions():
                    print(permission.user_login + ' - ' + str(PermissionType(permission.permission_type).name))
        close()

    @staticmethod
    def show_plan(args):
        connect()
        user = Actions.get_user_from_file(args)
        if args.id is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        issue = args.issue_class.find_by_id(args.id)
        if not issue.is_visible_to(user):
            print('You cannot watch this issue')
            close()
            return
        if issue is None:
            print('None')
            close()
            return

        print('Title: ' + issue.title)
        tags = issue.tags(user)
        if tags:
            print('Tags:')
            for tag in tags:
                print(tag)
        print('Description: ' + issue.description)
        print('Priority: ' + str(issue.priority))
        if issue.deadline_since_start is not None:
            print('Deadline since start: ' + (str(issue.deadline_since_start)))
            print('Deadline action: ' + str(DeadlineActions(issue.deadline_action).name))
        print('Owner: ' + issue.owner.login)

        if issue.is_managed_by(user):
            if issue.parent_issue is None:
                print('Parent: None')
            else:
                print('Parent: ' + str(issue.parent_issue.id) + ' - ' + issue.parent_issue.title)
            print('Children: ')
            for issue in issue.children_issues:
                print(str(issue.id) + ' - ' + issue.title)
            print('Alternatives: ')
            for issue in issue.alternatives:
                print(str(issue.id) + ' - ' + issue.title)
            print('Blocked by: ')
            for issue in issue.blocked_by:
                print(str(issue.id) + ' - ' + issue.title)
            print('Mutually exclusive: ')
            for issue in issue.mutually_exclusives:
                print(str(issue.id) + ' - ' + issue.title)
            if issue.is_managed_by(user):
                print('Permissions: ')
                for permission in issue.all_permissions():
                    print(permission.user_login + ' - ' + str(PermissionType(permission.permission_type).name))
        close()

    @staticmethod
    def create_issue(args):
        dead = args.deadline
        if dead is not None:
            dead = datetime.datetime.strptime(dead, '%d.%m.%y %H:%M:%S')
        args.deadline = dead
        connect()
        user = Actions.get_user_from_file(args)
        issue = user.new_issue(**vars(args))
        close()
        print('ID = ' + str(issue.id))

    @staticmethod
    def parse_time(time_str):
        regex = re.compile(r'((?P<days>\d+?)d)?((?P<hours>\d+?)h)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')
        parts = regex.match(time_str)
        if not parts:
            return
        parts = parts.groupdict()
        time_params = {}
        for attr in parts:
            if parts[attr] is not None:
                time_params[attr] = int(parts[attr])
        return datetime.timedelta(**time_params)

    @staticmethod
    def create_plan(args):
        deadline_from_start = args.deadline
        if deadline_from_start is not None:
            deadline_from_start = Actions.parse_time(deadline_from_start)
            if deadline_from_start is None:
                print('Incorrect deadline')
                close()
                return
        args.deadline_since_start = deadline_from_start
        connect()
        user = Actions.get_user_from_file(args)
        issue = user.new_plan(**vars(args))
        close()
        print('ID = ' + str(issue.id))

    @staticmethod
    def task_statuses(args):
        for i, status in enumerate(TaskStatus, 1):
            print(str(i) + '. ' + status.name)

    @staticmethod
    def period_types(args):
        for i, period in enumerate(PeriodType, 1):
            print(str(i) + '. ' + period.name)

    @staticmethod
    def permission_types(args):
        for i, permission in enumerate(PermissionType, 1):
            print(str(i) + '. ' + permission.name)

    @staticmethod
    def deadline_actions(args):
        for i, action in enumerate(DeadlineActions, 1):
            print(str(i) + '. ' + action.name)

    @staticmethod
    def modify_issue(args):
        connect()
        user = Actions.get_user_from_file(args)
        issue = Issue.find_by_id(args.id)
        for attr in ['title', 'description', 'priority', 'deadline_action']:
            if args.__getattribute__(attr) is not None:
                issue.__setattr__(attr, args.__getattribute__(attr))
        if args.deadline is not None:
            issue.deadline = datetime.datetime.strptime(args.deadline, '%d.%m.%y %H:%M:%S')
        issue.save(user)
        if args.status is not None:
            issue.set_status(args.status, user)
        close()

    @staticmethod
    def modify_plan(args):
        connect()
        user = Actions.get_user_from_file(args)
        issue = Plan.find_by_id(args.id)
        for attr in ['title', 'description', 'priority', 'deadline_action']:
            if args.__getattribute__(attr) is not None:
                issue.__setattr__(attr, args.__getattribute__(attr))
        if args.deadline is not None:
            issue.deadline_since_start = Actions.parse_time(args.deadline)
        issue.save(user)
        close()

    @staticmethod
    def add_alternative(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        alternative = args.issue_class.find_by_id(args.id2)
        issue.add_alternative(alternative, user)
        close()

    @staticmethod
    def remove_alternative(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        alternative = args.issue_class.find_by_id(args.id2)
        issue.remove_alternative(alternative, user)
        close()

    @staticmethod
    def add_blocking(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        blocking = args.issue_class.find_by_id(args.id2)
        issue.add_blocking_issue(blocking, user)
        close()

    @staticmethod
    def remove_blocking(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        blocking = args.issue_class.find_by_id(args.id2)
        issue.remove_blocking_issue(blocking, user)
        close()

    @staticmethod
    def add_exclusive(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        exclusive = args.issue_class.find_by_id(args.id2)
        issue.add_mutually_exclusive(exclusive, user)
        close()

    @staticmethod
    def remove_exclusive(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        exclusive = args.issue_class.find_by_id(args.id2)
        issue.remove_mutually_exclusive(exclusive, user)
        close()

    @staticmethod
    def set_parent_issue(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        parent = args.issue_class.find_by_id(args.id2)
        issue.set_parent_issue(parent, user)
        close()

    @staticmethod
    def set_permissions(args):
        if args.id is None or args.id2 is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user_who_grants_permissions = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        user = User.find_by_login(args.login)
        issue.set_permission_to(user, args.permission, user_who_grants_permissions)
        close()

    @staticmethod
    def create_schedule(args):
        if args.period_type is None or args.periods_between is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        _type = PeriodType(args.period_type)
        amount = args.periods_between
        connect()
        schedule = Schedule.create(_type, amount)
        if args.moments is not None:
            for moment in args.moments:
                try:
                    schedule.add_moment(moment)
                except Exception as e:
                    print(e)
                    schedule.delete()
                    close()
                    return
        close()
        print('ID = ' + str(schedule.id))

    @staticmethod
    def show_schedule(args):
        if args.id is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        schedule = Schedule.find_by_id(args.id)
        if schedule is None:
            print('None')
        print('Period type: ' + str(PeriodType(schedule.period_type).name))
        print('Periods cycle: ' + str(schedule.periods_between))
        date = datetime.datetime.strptime(args.datetime, '%d.%m.%y').date()
        print('Dates from ' + args.datetime + ':')
        count = len(schedule.moments)
        for i, d in enumerate(schedule.get_all_dates(date, args.amount_of_issues)):
            print(str(i // count + 1) + '.' + str(i % count + 1) + '. ' + str(d))
        close()

    @staticmethod
    def attach_plan_to_schedule(args):
        if args.id is None or args.plan_id or args.datetime or args.amount_of_issues is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        schedule = Schedule.find_by_id(args.id)
        if schedule is None:
            print('No schedule exists')
            return
        issue = Plan.find_by_id(args.plan_id)
        start_time = datetime.datetime.strptime(args.datetime, '%d.%m.%y %H:%M:%S')
        issue.add_schedule(schedule, start_time, args.amount_of_issues, user)
        close()

    @staticmethod
    def print_config_path(args):
        print(args.config)

    @staticmethod
    def reset_db(args):
        if os.path.exists(args.db_location):
            os.remove(args.db_location)
        lib.peewee_models.set_db()

    @staticmethod
    def add_tag(args):
        if args.tag is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        issue.add_tag(args.tag, user)
        close()

    @staticmethod
    def remove_tag(args):
        if args.tag is None:
            raise ValueError(MISSING_ARGS_ERR_MSG)
        connect()
        user = Actions.get_user_from_file(args)
        issue = args.issue_class.find_by_id(args.id)
        issue.remove_tag(args.tag, user)
        close()

    @staticmethod
    def show_log_levels(args):
        print('10. DEBUG')  # calls, args, returns, peewee
        print('20. INFO')  # calls, args, returns
        print('30. WARNINGS')
        print('40. ERROR')
        print('50. CRITICAL')

    @staticmethod
    def set_log_level(args):
        if args.level in [10*i for i in range(1, 6)]:
            with open(args.config) as input:
                config = json.load(input)
            config['log_level'] = args.level
            with open(args.config, 'w') as out:
                json.dump(config, out)
        else:
            print('INCORRECT LOGGING LEVEL')

    @staticmethod
    def run_tests(args):
        unittest.TextTestRunner(verbosity=2).run(test_suite.suite())


class App:
    commands = {
        ('user', 'create'): Actions.create_user,
        ('user', 'change_password'): Actions.change_password,
        ('user', 'change_login'): Actions.change_login,
        ('user', 'list'): Actions.show_users,

        ('settings', 'su'): Actions.log_in,
        ('settings', 'config'): Actions.print_config_path,
        ('settings', 'reset_db'): Actions.reset_db,
        ('settings', 'log_level'): Actions.set_log_level,
        ('settings', 'logging_levels'): Actions.show_log_levels,
        ('settings', 'test'): Actions.run_tests,

        ('issue', 'list'): Actions.issues_list,
        ('issue', 'show'): Actions.show_issue,
        ('issue', 'create'): Actions.create_issue,
        ('issue', 'statuses'): Actions.task_statuses,
        ('issue', 'deadline_actions'): Actions.deadline_actions,
        ('issue', 'permission_types'): Actions.permission_types,
        ('issue', 'update'): Actions.modify_issue,
        ('issue', 'set_parent'): Actions.set_parent_issue,
        ('issue', 'add_alt'): Actions.add_alternative,
        ('issue', 'add_block'): Actions.add_blocking,
        ('issue', 'add_exclusive'): Actions.add_exclusive,
        ('issue', 'rm_alt'): Actions.remove_alternative,
        ('issue', 'rm_block'): Actions.remove_blocking,
        ('issue', 'rm_exclusive'): Actions.remove_exclusive,
        ('issue', 'chmod'): Actions.set_permissions,
        ('issue', 'add_tag'): Actions.add_tag,
        ('issue', 'remove_tag'): Actions.remove_tag,

        ('plan', 'list'): Actions.issues_list,
        ('plan', 'show'): Actions.show_plan,
        ('plan', 'create'): Actions.create_plan,
        ('plan', 'deadline_actions'): Actions.deadline_actions,
        ('plan', 'permission_types'): Actions.permission_types,
        ('plan', 'update'): Actions.modify_plan,
        ('plan', 'set_parent'): Actions.set_parent_issue,
        ('plan', 'add_alt'): Actions.add_alternative,
        ('plan', 'add_block'): Actions.add_blocking,
        ('plan', 'add_exclusive'): Actions.add_exclusive,
        ('plan', 'rm_alt'): Actions.remove_alternative,
        ('plan', 'rm_block'): Actions.remove_blocking,
        ('plan', 'rm_exclusive'): Actions.remove_exclusive,
        ('plan', 'chmod'): Actions.set_permissions,
        ('plan', 'add_tag'): Actions.add_tag,
        ('plan', 'remove_tag'): Actions.remove_tag,

        ('schedule', 'period_types'): Actions.period_types,
        ('schedule', 'create'): Actions.create_schedule,
        ('schedule', 'show'): Actions.show_schedule,
        ('schedule', 'add_plan'):  Actions.attach_plan_to_schedule
    }


def main():
    parser = argparse.ArgumentParser(description='TaskTracker', prog='task')
    subparsers = parser.add_subparsers(dest='module')
    user_parser = subparsers.add_parser('user', help='Account commands', formatter_class=argparse.RawTextHelpFormatter,
                                        description='COMMANDS:\n'
                                        'create - creates new user. Requires: --login, --password(after '
                                        'user is created you need to perform "settings su" to select it)\n\n'
                                        'change_password - changes password of current user. '
                                        'Requires: --password\n\n'
                                        'change_login - changes login of current user. Requires: --login \n\n'
                                        'list - shows logins of all registered users')
    user_parser.add_argument('command')
    user_parser.add_argument('-l', '--login', action='store')
    user_parser.add_argument('-p', '--password', action='store')
    user_parser.add_argument('--config', action='store', help='Configuration file to use')

    settings_parser = subparsers.add_parser('settings', help='App settings',
                                            formatter_class=argparse.RawTextHelpFormatter,
                                            description='COMMANDS:\n'
                                            'su - select user. Requires: --login, --password\n\n'
                                            'config - shows location of config file, where database location and '
                                            'logs location can be specified\n\n'
                                            'reset_db - restores database to original state(deletes EVERYTHING). '
                                            'Perform this command after changing database location to create db\n\n'
                                            'log_level - set logging level\n\n'
                                            'logging_levels - shows list of available logging levels\n\n'
                                            'test - performs some tests for application')
    settings_parser.add_argument('command')
    settings_parser.add_argument('-l', '--login', action='store')
    settings_parser.add_argument('-p', '--password', action='store')
    settings_parser.add_argument('-lvl', '--level', action='store', help='Desired logging level', type=int)
    settings_parser.add_argument('--config', action='store', help='Configuration file to use')

    issue_parser = subparsers.add_parser('issue', help='Issue operations',
                                         formatter_class=argparse.RawTextHelpFormatter,
                                         description='COMMANDS:\n'
                                         'list - shows list of issues, that visible to you. Some filters can be '
                                         'applied. Optional: --title, --description, --priority, --tag, --status\n\n'
                                         'show - shows information on concrete issue. Required -id\n\n'
                                         'create - creates new issue. Required: --title, --description, --priority'
                                         ' Optional: --deadline, --deadline_action\n\n'
                                         'statuses - shows list of available issue statuses\n\n'
                                         'deadline_actions - shows list of available actions on deadlines\n\n'
                                         'update - modifies concrete issue. Required: -id Optional: --title, '
                                         '--description, --priority, --deadline, --deadline_action, --status\n\n'
                                         'set_parent - gives issue parent issue. Required: -id, -id2 - id of '
                                         'parent-issue(if not specified issue will have no parent)\n\n'
                                         'add_alt - adds alternative issue. Required: -id, -id2 - id'
                                         ' of alternative issue\n\n'
                                         'add_block - adds blocking issue. Required: -id, -id2 - id'
                                         ' of blocking issue\n\n'
                                         'add_exclusive - adds mutually exclusive issue. Required: -id, -id2 - id'
                                         ' of mutually exclusive issue\n\n'
                                         'rm_alt, rm_block, rm_exclusive - remove issue from concrete role'
                                         'Required: -id, -id2\n\n'
                                         'permission_types - shows list of available permission levels\n\n'
                                         'chmod - grants another user permission to interact with particular issue'
                                         ' and all its children. Required: -id, --login, --permission,\n\n'
                                         'add_tag, remove_tag - adds/removes tag. Required: -id, --tag')
    issue_parser.add_argument('command')
    issue_parser.add_argument('-id', help='ID of issue', type=int)
    issue_parser.add_argument('-id2', help='ID of issue used in operation', type=int)
    issue_parser.add_argument('-t', '--title', help='Title of issue')
    issue_parser.add_argument('-d', '--description', help='Description of issue')
    issue_parser.add_argument('-p', '--priority', help='Priority of issue', type=int)
    issue_parser.add_argument('-s', '--status', help='Status of issue', type=int)
    issue_parser.add_argument('-l', '--login', action='store')
    issue_parser.add_argument('--tag', action='store')
    issue_parser.add_argument('--deadline', help='Deadline of issue')
    issue_parser.add_argument('--deadline_action', help='Action on deadline of issue')
    issue_parser.add_argument('--permission', help='Permission type', type=int)
    issue_parser.add_argument('--config', action='store', help='Configuration file to use')

    plan_parser = subparsers.add_parser('plan', help='Planned issues operations',
                                         formatter_class=argparse.RawTextHelpFormatter,
                                         description='COMMANDS:\n'
                                         'list - shows list of plans, that visible to you. Some filters can be '
                                         'applied. Optional: --title, --description, --priority, --tag\n\n'
                                         'show - shows information on concrete plan. Required -id\n\n'
                                         'create - creates new plan. Required: --title, --description, --priority'
                                         ' Optional: --deadline, --deadline_action\n\n'
                                         'deadline_actions - shows list of available actions on deadlines\n\n'
                                         'update - modifies concrete plan. Required: -id Optional: --title, '
                                         '--description, --priority, --deadline, --deadline_action\n\n'
                                         'set_parent - gives plan parent plan. Required: -id, -id2 - id of '
                                         'parent-plan(if not specified plan will have no parent)\n\n'
                                         'add_alt - adds alternative plan. Required: -id, -id2 - id'
                                         ' of alternative plan\n\n'
                                         'add_block - adds blocking plan. Required: -id, -id2 - id'
                                         ' of blocking plan\n\n'
                                         'add_exclusive - adds mutually exclusive plan. Required: -id, -id2 - id'
                                         ' of mutually exclusive plan\n\n'
                                         'rm_alt, rm_block, rm_exclusive - remove plan from concrete role'
                                         'Required: -id, -id2\n\n'
                                         'permission_types - shows list of available permission levels\n\n'
                                         'chmod - grants another user permission to interact with particular plan\n\n'
                                         ' and all its children. Required: -id, --login, --permission,\n\n'
                                         'add_tag, remove_tag - adds/removes tag. Required: -id, --tag')
    plan_parser.add_argument('command', help='list, show, create, deadline_actions, update, set_parent, add_alt, '
                                             'add_block, add_exclusive, rm_alt, rm_block, rm_exclusive, '
                                             'permission_types, chmod, add_tag, remove_tag')
    plan_parser.add_argument('-id', help='ID of plan', type=int)
    plan_parser.add_argument('-id2', help='ID of plan used in operation', type=int)
    plan_parser.add_argument('-t', '--title', help='Title of plan')
    plan_parser.add_argument('--tag', action='store')
    plan_parser.add_argument('-d', '--description', help='Description of plan')
    plan_parser.add_argument('-p', '--priority', help='Priority of plan', type=int)
    plan_parser.add_argument('-l', '--login', action='store')
    plan_parser.add_argument('--deadline', help='Deadline since creation of issue: 1d1h1m1s')
    plan_parser.add_argument('--deadline_action', help='Action on deadline of issue')
    plan_parser.add_argument('--permission', help='Permission type', type=int)
    plan_parser.add_argument('--config', action='store', help='Configuration file to use')

    schedule_parser = subparsers.add_parser('schedule', help='Schedule operations',
                                            formatter_class=argparse.RawTextHelpFormatter,
                                            description='COMMANDS:\n'
                                            'create - creates new schedule. Required: --period_type, --periods_between,'
                                                        ' --moments\n\n'
                                            'show - shows all dates of schedule. Required: -id, --amount_of_issues'
                                                        ' Optional: --datetime\n\n'
                                            'period_types - shows list of available period types\n\n'
                                            'add_plan - attaches plan to schedule. Required: -id, --plan_id,'
                                                        ' --datetime, --amount_of_issues')
    schedule_parser.add_argument('command', help='create, show, period_types, add_plan')
    schedule_parser.add_argument('-id', help='ID of schedule', type=int)
    schedule_parser.add_argument('-t', '--period_type', help='Period type', type=int)
    schedule_parser.add_argument('-p', '--periods_between', help='Amount of periods passed '
                                                                 'between two issue-creation cycles', type=int)
    schedule_parser.add_argument('-m', '--moments', help='One moment = one day, 0 = today, 1 '
                                                         '= tomorrow...', nargs='*', type=int)
    schedule_parser.add_argument('-a', '--amount_of_issues', help='Amount of dates in show', type=int)
    schedule_parser.add_argument('-d', '--datetime', help='Date and time of first plan creation')
    schedule_parser.add_argument('-pid', '--plan_id', help='ID of adding plan')
    schedule_parser.add_argument('--config', action='store', help='Configuration file to use')

    parsers = {
        'user': user_parser,
        'issue': issue_parser,
        'plan': plan_parser,
        'settings': settings_parser,
        'schedule': schedule_parser
    }

    args = parser.parse_args()

    if args.config is None:
        args.config = DEFAULT_CONFIG_PATH
        if not os.path.exists(DEFAULT_CONFIG_PATH):
            dir_path = os.path.join(HOME, APP_NAME)
            config = {
                'db_location': os.path.join(HOME, APP_NAME, 'tasktracker.db'),
                'log_location': os.path.join(HOME, APP_NAME, 'tasktracker.log'),
                'login': 'liam',
                'password': 'liam',
                'log_level': 20
            }
            os.makedirs(dir_path, exist_ok=True)
            with open(DEFAULT_CONFIG_PATH, 'w') as out:
                json.dump(config, out)

    try:
        with open(args.config) as input:
            config = json.load(input)
            logger = lib.logging.get_logger()
            file_handler = logging.FileHandler(config['log_location'])
            file_handler.setLevel(config['log_level'])
            logger.addHandler(file_handler)
            logger.debug(str(args))
            change_database(config['db_location'])
            args.db_location = config['db_location']
        if args.module == 'issue':
            args.issue_class = Issue
        elif args.module == 'plan':
            args.issue_class = Plan
        function = (args.module, args.command)
        App.commands[function](args)
    except Exception as e:
        print('ERROR OCCURRED: ', file=sys.stderr)
        print(e, file=sys.stderr)
        parsers[args.module].print_help(sys.stderr)
        sys.exit(1)

if __name__ == '__main__':
    main()
