import datetime
from enum import IntEnum
from lib.logging import log_call
from lib import db_interface, issue_base

NONE_FIELD_ERR_MSG = "Title, description and priority must be specified"
WRONG_DEADLINE_ACTION_ERR_MSG = "Incorrect deadline action"


class DeadlineActions(IntEnum):
    NOTHING = 1
    SET_AS_CRITICAL = 2
    SET_AS_FAILED = 3


class TaskStatus(IntEnum):
    NOT_STARTED = 1
    ON_THE_GO = 2
    INFORMATION_REQUIRED = 3
    COMPLETED = 4
    FAILED = 5


class Issue(issue_base.IssueBase):

    """
        This class contains common issue operations.
        Do not use ctor to create issue. Use Issue.create() instead.
        Issue must have title, description and priority defined, besides situation when issue has link to Plan
    """

    @classmethod
    def _actions_(cls):
        return db_interface.IssueDBActions

    @classmethod
    def _tag_actions_(cls):
        return db_interface.TagIssueDBActions

    @classmethod
    def _bindings_actions_(cls):
        return db_interface.IssueBindingsDBActions

    @classmethod
    def _bindings_class_(cls):
        return IssueBindings

    @classmethod
    def _permissions_actions_(cls):
        return db_interface.IssuePermissionsDBActions

    @staticmethod
    @log_call
    def create(title, description, priority, deadline, deadline_action, owner_id,
               plan_id=None, creation_time=None):
        """
            Saves to database new object of Issue.
            Title, description and priority must be defined at least in Plan, otherwise exception will be thrown
            :return: Issue
        """
        if creation_time is None:
            creation_time = datetime.datetime.now()
        instance = Issue(title, description, priority, deadline, deadline_action, creation_time, owner_id, None,
                         plan_id)
        instance._validate()
        instance.id = db_interface.IssueDBActions.create(instance).id
        return instance

    def _raw_getattribute_(self, attr):
        return super().__getattribute__(attr)

    def __init__(self, title, description, priority, deadline, deadline_action, creation_time, owner_id, id=None,
                 plan_id=None):
        self.plan_id = plan_id
        self.title = title
        self.description = description
        self.priority = priority
        self.deadline = deadline
        self.creation_time = creation_time
        self.deadline_action = deadline_action
        self.owner_id = owner_id
        if id is not None:
            self.id = id

    def _validate(self):
        if self.title is None or self.description is None or self.priority is None:
            raise ValueError(NONE_FIELD_ERR_MSG)
        if self.creation_time > datetime.datetime.now():
            raise ValueError("Creation time must be less then now")
        if self.deadline_action is not None and not issue_base.EnumHelper.contains(DeadlineActions, self.deadline_action):
            return ValueError(WRONG_DEADLINE_ACTION_ERR_MSG)

    @log_call
    def delete(self):
        """
            Deletes concrete issue (self), all its links with other issues and all associated permissions.
        """
        for link in self._links_():
            link.delete()
        for permission in self._permissions_actions_().find_by_issue_id(self.id):
            permission.delete()
        db_interface.IssueDBActions.delete(self.id)
        self._clear_tags_()

    @log_call
    def _save_priority_(self):
        db_interface.IssueDBActions.save_priority(self)

    @log_call
    def _save_deadline_action_(self):
        db_interface.IssueDBActions.save_deadline_action(self)

    @log_call
    def check_deadline(self):
        """
            Checks if deadline has passed and, if so reacts accordingly.
        """
        status = self._get_status_()
        if status == TaskStatus.COMPLETED or status == TaskStatus.FAILED:
            return
        if self.deadline is not None and self.deadline < datetime.datetime.now():
            if self.deadline_action == DeadlineActions.SET_AS_CRITICAL:
                self.priority = 100
                self._save_priority_()
            elif self.deadline_action == DeadlineActions.SET_AS_FAILED:
                self._set_status_(TaskStatus.FAILED)
            self.deadline_action = DeadlineActions.NOTHING
            self._save_deadline_action_()

    @log_call
    def save(self, user):
        """
            Saves changes to this issue (self) to database.
            Raises exception if title or description or priority is None.
        """
        if not self.is_managed_by(user):
            raise PermissionError(issue_base.NO_PERMISSION_ERR_MSG)
        self._validate()
        db_interface.IssueDBActions.save(self)

    @log_call
    def _save_tdp_(self):
        db_interface.IssueDBActions.save_tdp(self)

    @log_call
    def get_status(self, user):
        """
            Reads from database status of current issue (self).
            :return: TaskStatus.
        """
        if not self.is_spectated_by(user):
            raise PermissionError(issue_base.NO_PERMISSION_ERR_MSG)
        return self._get_status_()

    @log_call
    def _get_status_(self):
        return db_interface.IssueDBActions.get_status(self.id)

    @property
    @log_call
    def plan(self):
        """
            Takes from database Plan associated with this issue (self).
            :return: Plan
        """
        id = self.plan_id
        if id is not None:
            return db_interface.PlanDBActions.find_by_id(id)
        else:
            return None

    @property
    @log_call
    def owner(self):
        """
            Selects from database User, whose id is owner_id of concrete Issue (self).
            :return: User
        """
        id = self.owner_id
        return db_interface.UserDBActions.find_by_id(id)

    @log_call
    def _plan_deleting_(self):
        plan = self.plan
        self.plan_id = None
        if self.title is None:
            self.title = plan.title
        if self.description is None:
            self.description = plan.description
        if self.priority is None:
            self.priority = plan.priority
        if self.owner_id is None:
            self.owner_id = plan.owner_id
        self._save_tdp_()

    def __getattribute__(self, item):
        attr = super().__getattribute__(item)
        if attr is not None:
            return attr
        else:
            if item == 'title' or item == 'description' or item == 'priority' or item == 'owner_id':
                plan = self.plan
                if plan is not None:
                    return plan.__getattribute__(item)
            return None

    def set_parent_issue(self, new_parent, user):
        if new_parent is not None:
            if new_parent._get_status_() == TaskStatus.COMPLETED and self._get_status_() != TaskStatus.COMPLETED:
                raise ValueError('Cannot attach uncompleted task to completed')

            if new_parent._get_status_() != TaskStatus.FAILED and self._get_status_() == TaskStatus.FAILED:
                raise ValueError('Cannot attach failed task to not failed')
        super().set_parent_issue(new_parent, user)

    def add_blocking_issue(self, blocking, user):
        if blocking is None:
            return
        if blocking._get_status_() == TaskStatus.FAILED and self._get_status_() != TaskStatus.FAILED:
            raise ValueError('Failed issue cannot block not failed')

        if self._get_status_() == TaskStatus.COMPLETED and blocking._get_status_() != TaskStatus.COMPLETED:
            raise ValueError('Uncompleted issue cannot block completed')
        super().add_blocking_issue(blocking, user)

    @log_call
    def _has_failed_children_(self):
        for child in self.children_issues:
            if child._get_status_() == TaskStatus.FAILED:
                for child_alternative in child.alternatives:
                    if child_alternative._get_status_() != TaskStatus.FAILED:
                        break
                else:
                    return True
        return False

    @log_call
    def _has_uncomplited_children_(self):
        for child in self.children_issues:
            if child._get_status_() != TaskStatus.COMPLETED:
                for child_alternative in child.alternatives:
                    if child_alternative._get_status_() == TaskStatus.COMPLETED:
                        break
                else:
                    return True
        return False

    def _update_parent_status_(self):
        parent = self.parent_issue
        if parent is not None:
            if parent._has_failed_children_():
                parent._set_status_(TaskStatus.FAILED)
                return

            if parent._has_uncomplited_children_() and parent._get_status_() == TaskStatus.COMPLETED:
                parent._set_status_(TaskStatus.ON_THE_GO)

    def add_mutually_exclusive(self, exclusive, user):
        if exclusive is None:
            return
        if self._get_status_() == TaskStatus.COMPLETED and exclusive._get_status_() != TaskStatus.FAILED:
            raise ValueError('No exclusion between completed and not failed tasks is allowed')

        if exclusive._get_status_() == TaskStatus.COMPLETED and self._get_status_() != TaskStatus.FAILED:
            raise ValueError('No exclusion between completed and not failed tasks is allowed')
        super().add_mutually_exclusive(exclusive, user)

    @log_call
    def set_status(self, new_status, user):
        """
            Updates status of this issue (self) in database.
            :param new_status: TaskStatus
        """
        if not self.is_participated_by(user):
            raise PermissionError(issue_base.NO_PERMISSION_ERR_MSG)
        self._set_status_(new_status)

    @log_call
    def _set_status_(self, new_status):
        if new_status == self._get_status_():
            return
        if not issue_base.EnumHelper.contains(TaskStatus, new_status):
            raise ValueError('Incorrect status')
        old_status = self._get_status_()

        if old_status == TaskStatus.FAILED:
            if self._has_failed_children_():
                raise ValueError('Task has failed children issues')

            for block in self.blocked_by:
                if block._get_status_() != TaskStatus.COMPLETED:
                    raise ValueError('Task has failed blocking tasks')

            for exclusive in self.mutually_exclusives:
                if exclusive._get_status_() == TaskStatus.COMPLETED:
                    raise ValueError('Task has mutually excluding completed task')

        if new_status == TaskStatus.COMPLETED:
            if self._has_uncomplited_children_():
                raise ValueError('Task has uncompleted children-tasks')

            for block in self.blocked_by:
                if block._get_status_() != TaskStatus.COMPLETED:
                    raise ValueError('Task has uncompleted blocking tasks')

            exclusives = self.mutually_exclusives
            db_interface.IssueDBActions.set_status(self.id, TaskStatus.COMPLETED)
            for exclusive in exclusives:
                exclusive._set_status_(TaskStatus.FAILED)

        elif new_status == TaskStatus.FAILED:
            db_interface.IssueDBActions.set_status(self.id, TaskStatus.FAILED)
            self._update_parent_status_()
            for blocked in self.blocks_following:
                blocked._set_status_(TaskStatus.FAILED)

        else:
            db_interface.IssueDBActions.set_status(self.id, new_status)
            if old_status == TaskStatus.COMPLETED:
                self._update_parent_status_()
                for blocked in self.blocks_following:
                    if blocked._get_status_() == TaskStatus.COMPLETED:
                        blocked._set_status_(TaskStatus.ON_THE_GO)


class IssueBindings(issue_base.IssueBindingsBase):

    @classmethod
    def _actions_(cls):
        return db_interface.IssueBindingsDBActions

    def __init__(self, first_id, second_id, link):
        super().__init__(first_id, second_id, link)

    @staticmethod
    @log_call
    def create_many(bindings_list):
        db_interface.IssueBindingsDBActions.create_many(bindings_list)
