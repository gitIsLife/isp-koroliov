
"""
    Data models to work with peewee are defined here.
    set_db() creates all tables and fills enums
"""

from peewee import (
    Model,
    SqliteDatabase,
    CharField,
    ForeignKeyField,
    SmallIntegerField,
    TimestampField,
    IntegerField,
    DateTimeField,
    CompositeKey,
    Proxy
)

database_proxy = Proxy()
database_proxy.initialize(SqliteDatabase("../../task_tracker.db"))


def use_database(db_location):
    database_proxy.initialize(SqliteDatabase(db_location))


class PeriodTypeModel(Model):
    period_type = CharField(unique=True)

    class Meta:
        database = database_proxy


class PermissionTypeModel(Model):
    permission = CharField(unique=True)

    class Meta:
        database = database_proxy


class DeadlineActionTypeModel(Model):
    action = CharField(unique=True)

    class Meta:
        database = database_proxy


class TaskStatusTypeModel(Model):
    status = CharField(unique=True)

    class Meta:
        database = database_proxy


class TaskLinkModel(Model):
    link = CharField(unique=True)

    class Meta:
        database = database_proxy


class UserModel(Model):
    login = CharField(unique=True)
    password = CharField()
    notifications_settings = SmallIntegerField()
    notifications_priority_bound = SmallIntegerField()

    class Meta:
        database = database_proxy


class TagModel(Model):
    tag = CharField(unique=True)

    class Meta:
        database = database_proxy


class PlanModel(Model):
    title = CharField()
    description = CharField()
    priority = SmallIntegerField(default=50)
    owner_id = ForeignKeyField(UserModel)
    deadline_since_start = TimestampField(null=True)
    deadline_action = ForeignKeyField(DeadlineActionTypeModel, null=True)

    class Meta:
        database = database_proxy


class TagPlanLinkModel(Model):
    tag = ForeignKeyField(TagModel)
    issue = ForeignKeyField(PlanModel)
    user = ForeignKeyField(UserModel)

    class Meta:
        database = database_proxy


class ScheduleModel(Model):
    period_type = ForeignKeyField(PeriodTypeModel)
    periods_between = IntegerField()

    class Meta:
        database = database_proxy


class IssueModel(Model):
    plan_id = ForeignKeyField(PlanModel, related_name='created_issues', null=True)
    title = CharField(null=True)
    description = CharField(null=True)
    priority = SmallIntegerField(default=50, null=True)
    deadline = DateTimeField(null=True)
    creation_time = DateTimeField()
    deadline_action = ForeignKeyField(DeadlineActionTypeModel, null=True)
    owner_id = ForeignKeyField(UserModel)
    status = ForeignKeyField(TaskStatusTypeModel)

    class Meta:
        database = database_proxy


class TagIssueLinkModel(Model):
    tag = ForeignKeyField(TagModel)
    issue = ForeignKeyField(IssueModel)
    user = ForeignKeyField(UserModel)

    class Meta:
        database = database_proxy


class IssueBindingsModel(Model):
    first_id = ForeignKeyField(IssueModel, backref='bindings_first')
    second_id = ForeignKeyField(IssueModel, backref='bindings_second')
    link = ForeignKeyField(TaskLinkModel)

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("first_id", "second_id")


class ScheduleTimeMomentModel(Model):
    schedule = ForeignKeyField(ScheduleModel, related_name='moments')
    moment = IntegerField()

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("schedule", "moment")


class SchedulePlanLinkModel(Model):
    first_creation_time = DateTimeField()
    total_amount_of_tasks = IntegerField()
    issue_id = ForeignKeyField(PlanModel)
    schedule_id = ForeignKeyField(ScheduleModel)

    class Meta:
        database = database_proxy


class PlanBindingsModel(Model):
    first_id = ForeignKeyField(PlanModel)
    second_id = ForeignKeyField(PlanModel)
    link = ForeignKeyField(TaskLinkModel)

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("first_id", "second_id")


class ReminderModel(Model):
    issue = ForeignKeyField(IssueModel)
    user = ForeignKeyField(UserModel)

    class Meta:
        database = database_proxy


class ReminderTimeLinkModel(Model):
    first_reminding_time = DateTimeField()
    total_amount_of_reminds = IntegerField()
    reminder = ForeignKeyField(ReminderModel)
    schedule = ForeignKeyField(ScheduleModel)
    # if first_reminding_time < DReminder_limit then it's DReminder

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("reminder", "schedule")


class ScheduledReminderModel(Model):
    issue = ForeignKeyField(PlanModel)
    user = ForeignKeyField(UserModel)

    class Meta:
        database = database_proxy


class ScheduledReminderTimeLinkModel(Model):
    first_reminding_time = DateTimeField()
    total_amount_of_reminds = IntegerField()
    reminder = ForeignKeyField(ScheduledReminderModel)
    schedule = ForeignKeyField(ScheduleModel)

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("reminder", "schedule")


class CommentModel(Model):
    user = ForeignKeyField(UserModel)
    issue = ForeignKeyField(IssueModel)
    time = DateTimeField()
    text = CharField()

    class Meta:
        database = database_proxy


class PlanCommentModel(Model):
    user = ForeignKeyField(UserModel)
    issue = ForeignKeyField(PlanModel)
    time = DateTimeField()
    text = CharField()

    class Meta:
        database = database_proxy


class IssuePermissionsModel(Model):
    user = ForeignKeyField(UserModel)
    issue = ForeignKeyField(IssueModel)
    permission = ForeignKeyField(PermissionTypeModel)
    notifications_settings = SmallIntegerField(null=True)

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("user", "issue")


class PlanPermissionsModel(Model):
    user = ForeignKeyField(UserModel)
    issue = ForeignKeyField(PlanModel)
    permission = ForeignKeyField(PermissionTypeModel)
    notifications_settings = SmallIntegerField(null=True)

    class Meta:
        database = database_proxy
        primary_key = CompositeKey("user", "issue")


def set_db():

    UserModel.create_table()
    DeadlineActionTypeModel.create_table()
    PlanModel.create_table()
    TaskStatusTypeModel.create_table()
    TaskLinkModel.create_table()
    PeriodTypeModel.create_table()
    TagModel.create_table()
    PlanModel.create_table()
    TagPlanLinkModel.create_table()
    ScheduleModel.create_table()
    IssueModel.create_table()
    TagIssueLinkModel.create_table()
    IssueBindingsModel.create_table()
    ScheduleTimeMomentModel.create_table()
    SchedulePlanLinkModel.create_table()
    PlanBindingsModel.create_table()
    ReminderModel.create_table()
    ReminderTimeLinkModel.create_table()
    ScheduledReminderModel.create_table()
    ScheduledReminderTimeLinkModel.create_table()
    CommentModel.create_table()
    PlanCommentModel.create_table()
    PermissionTypeModel.create_table()
    IssuePermissionsModel.create_table()
    PlanPermissionsModel.create_table()

    DeadlineActionTypeModel.create(action='NOTHING')
    DeadlineActionTypeModel.create(action='SET_AS_CRITICAL')
    DeadlineActionTypeModel.create(action='SET_AS_FAILED')

    TaskStatusTypeModel.create(status='NOT_STARTED')
    TaskStatusTypeModel.create(status='ON_THE_GO')
    TaskStatusTypeModel.create(status='INFORMATION_REQUIRED')
    TaskStatusTypeModel.create(status='COMPLETED')
    TaskStatusTypeModel.create(status='FAILED')

    TaskLinkModel.create(link='not_connected')
    TaskLinkModel.create(link='PARENT_CHILD')
    TaskLinkModel.create(link='BLOCKED_BLOCKING')
    TaskLinkModel.create(link='ALTERNATIVE')
    TaskLinkModel.create(link='MUTUALLY_EXCLUSIVE')

    PeriodTypeModel.create(period_type='DAILY')
    PeriodTypeModel.create(period_type='WEEKLY')
    PeriodTypeModel.create(period_type='MONTHLY')
    PeriodTypeModel.create(period_type='ANNUALLY')

    PermissionTypeModel.create(permission='NO')
    PermissionTypeModel.create(permission='SPECTATOR')
    PermissionTypeModel.create(permission='CONSULTANT')
    PermissionTypeModel.create(permission='PARTICIPANT')
    PermissionTypeModel.create(permission='MANAGER')
    PermissionTypeModel.create(permission='ADMIN')
