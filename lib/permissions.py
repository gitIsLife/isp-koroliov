from lib import db_interface
from lib.logging import log_call


class PermissionBase:

    """
        This class works with user-permissions.
    """

    @classmethod
    def interface(cls):
        pass

    @log_call
    def __init__(self, issue_id, user_id, permission_type):
        self.issue_id = issue_id
        self.user_id = user_id
        self.permission_type = permission_type

    @classmethod
    @log_call
    def create_many(cls, permissions):
        cls.interface().create_many(permissions)

    @log_call
    def delete(self):
        self.interface().delete(self)


class IssuePermissions(PermissionBase):

    @classmethod
    def interface(cls):
        return db_interface.IssuePermissionsDBActions


class PlanPermissions(PermissionBase):

    @classmethod
    def interface(cls):
        return db_interface.PlanPermissionsDBActions