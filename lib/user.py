import datetime
import hashlib
from lib import plan, db_interface, issue
from lib.logging import log_call

USER_WRONG_LOGIN_ERR_MSG = 'User with such login already exists'


class User:

    """
        Class contains basic operations with user, such as creation and deletion
    """

    @staticmethod
    def _get_hash_(password_string):
        password = bytearray(password_string, "utf-8")
        return hashlib.sha256(password).hexdigest()

    def __repr__(self):
        string = self.__class__.__name__ + '<'
        for attr in vars(self):
            string += "'" + attr + "': " + str(self.__getattribute__(attr)) + ', '
        return string[:-2] + '>'

    def __init__(self, login, password, id, notifications_bound, notifications_settings):
        self.login = login
        self.__password = password
        self.id = id
        self.notifications_bound = notifications_bound
        self.notifications_settings = notifications_settings

    @staticmethod
    @log_call
    def log_in(login, password):
        """
            Tries to get User object from database with specified login, and, if success compares password from database
            with specified password and, if passwords match returns User object, else returns None
            :return: User or None
        """
        user = db_interface.UserDBActions.find_by_login(login)
        if user is not None:
            if user.__password == User._get_hash_(password):
                user.check_deadlines_and_plans()
                return user
        return None

    @staticmethod
    @log_call
    def create(login, password):
        """
            Add user with specified parameters to database. If user with such login already exists throws exception
        """
        user = User(login, User._get_hash_(password), 0, 0, 0)
        if db_interface.UserDBActions.find_by_login(login) is not None:
            raise ValueError(USER_WRONG_LOGIN_ERR_MSG)
        db_interface.UserDBActions.create(user)

    @log_call
    def new_issue(self, title, description, priority, deadline, deadline_action, **unused):
        """
            Creates Issue object and sets its owner User object (self)
            Look Issue.create() for more information
            :return: Issue
        """
        return issue.Issue.create(title, description, priority, deadline, deadline_action, self.id)

    @log_call
    def new_plan(self, title, description, priority, deadline_since_start, deadline_action, **unused):
        """
            Creates Plan object and sets its owner User object (self)
            Look Plan.create() for more information
            :return: Plan
        """
        return plan.Plan.create(title, description, self.id, deadline_since_start,
                                deadline_action, priority)

    @property
    @log_call
    def owned_issues(self):
        """
            Finds all issues, where owner is current User (self)
            :return: [Issue]
        """
        return db_interface.IssueDBActions.find_by_owner(self.id)

    @property
    @log_call
    def owned_plans(self):
        """
            Finds all plans, where owner is current User (self)
            :return: [Plan]
        """
        return db_interface.PlanDBActions.find_by_owner(self.id)

    @property
    @log_call
    def password(self):
        return self.__password

    @password.setter
    @log_call
    def password(self, new):
        self.__password = User._get_hash_(new)

    def notify_on(self, notification):
        if self.notifications_settings & notification is not 0:
            return True
        else:
            return False

    def add_notification(self, notification):
        if not self.notify_on(notification):
            self.notifications_settings += notification

    def remove_notification(self, notification):
        if self.notify_on(notification):
            self.notifications_settings -= notification

    @log_call
    def save(self):
        """
            Saves changes of current User object. Including: login, password and notification settings.
            May throw an exception in case of login conflict.
        """
        user_login = db_interface.UserDBActions.find_by_login(self.login)
        if user_login is not None and user_login.id != self.id:
            raise ValueError(USER_WRONG_LOGIN_ERR_MSG)
        db_interface.UserDBActions.save(self)

    @log_call
    def delete(self):
        """
            Delete current user from database and all its owned issues and plans.
        """
        db_interface.PlanPermissionsDBActions.delete_user_permissions(self)
        db_interface.IssuePermissionsDBActions.delete_user_permissions(self)
        for task in self.owned_issues:
            task.delete()
        for plan in self.owned_plans:
            plan.delete()
        db_interface.UserDBActions.delete(self)

    @log_call
    def check_deadlines_and_plans(self):
        """
            Fails all user-visible deadlines and creates all issues from user-visible plans.
            If log_in() returns user, it also calls this method.
        """
        plans = plan.Plan.get_visible_to(self)
        representatives_ids = {plan._get_graph_ids_and_representative()[1] for plan in plans}
        representatives = db_interface.PlanDBActions.find_by_ids(representatives_ids)
        for _plan in representatives:
            dates = []
            for schedule in _plan.schedules:
                dates.extend(schedule.get_all_dates())
            for date in dates:
                if date < datetime.datetime.now():
                    for task in _plan.issues:
                        if task.creation_time == date:
                            break
                    else:
                        _plan._create_bindings_graph_(date)
        related_ids = set()
        for task in issue.Issue.get_visible_to(self):
            ids, repr = task._get_graph_ids_and_representative()
            related_ids.update(ids)
        for task in db_interface.IssueDBActions.find_by_ids(related_ids):
            task.check_deadline()

    @staticmethod
    def all_logins():
        return db_interface.UserDBActions.get_all_logins()

    @staticmethod
    @log_call
    def find_by_login(login):
        return db_interface.UserDBActions.find_by_login(login)
