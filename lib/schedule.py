import datetime
from enum import IntEnum
from lib.logging import log_call
from lib import db_interface, issue_base


class PeriodType(IntEnum):
    DAILY = 1
    WEEKLY = 2
    MONTHLY = 3
    ANNUALLY = 4


class Schedule:

    """
        This class specifies how planned issue will be created.
        Schedule: period_type = WEEKLY, periods_between = 2, first_creation_time = today, moments = [1, 3, 9]
        Issue will be created: 1) today, tomorrow, second day after tomorrow, eights day after tomorrow
                               2) today + 2 weeks, tomorrow + 2 weeks ...
        If issue must be created on 29th of every month, in february it will be created on 28th, except bissextiles.
    """

    @log_call
    def __init__(self, period_type, periods_between, id=None, first_creation_time=None, total_amount_of_tasks=None):
        self.periods_between = periods_between
        self.period_type = period_type
        if id is not None:
            self.id = id
        if first_creation_time is not None:
            self.first_creation_time = first_creation_time
            self.total_amount_of_tasks = total_amount_of_tasks

    def _validate(self):
        if not issue_base.EnumHelper.contains(PeriodType, self.period_type):
            raise ValueError("Incorrect period type")
        if self.periods_between < 1:
            raise ValueError("Periods between must be N")

    @staticmethod
    @log_call
    def create(period_type, periods_between=1):
        """
            Creates in database new object of schedule.
            :param period_type: must be PeriodType
            :param periods_between: must be int >= 1
            :return: Schedule object
        """
        inst = Schedule(period_type, periods_between)
        inst._validate()
        inst.id = db_interface.ScheduleDBActions.create(inst).id
        return inst

    @property
    @log_call
    def moments(self):
        """
            Returns time moments associated with this schedule from database with additional zero moment.
        """
        moments = db_interface.ScheduleDBActions.get_moments(self)
        moments.append(0)
        moments.sort()
        return moments

    @log_call
    def add_moment(self, moment):
        """
            Links specified moment in database with schedule (self).
            :param moment: must be limit > int > 0, where limit is number of days in time period with length of self.periods_between of self.period_type.
        """
        period = PeriodType
        period_const = 365
        if self.period_type == period.DAILY:
            period_const = 1
        elif self.period_type == period.WEEKLY:
            period_const = 7
        elif self.period_type == period.MONTHLY:
            period_const = 30
        limit = period_const * self.periods_between
        if moment >= limit or moment < 1:  # 0 always exists
            raise ValueError('Incorrect time moment, limit = ' + str(limit))
        if moment in self.moments:
            return
        db_interface.ScheduleDBActions.add_moment(self, moment)

    @log_call
    def remove_moment(self, moment):
        """
            Removes link of specified moment with schedule (self) in database.
        """
        db_interface.ScheduleDBActions.remove_moment(self, moment)

    @log_call
    def delete(self):
        """
            Deletes concrete (self) schedule and all its moments from database.
        """
        db_interface.ScheduleDBActions.delete(self)

    @classmethod
    @log_call
    def _find_day_(cls, year, month, day):
        days = {1: 31, 2: 29, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}
        if day > days[month]:
            day = days[month]
        try:
            return datetime.date(year=year, month=month, day=day)
        except:
            return datetime.date(year=year, month=month, day=day-1)  # 29 february

    @log_call
    def _get_dates_daily_(self, starting_time, amount_of_dates):
        delta = datetime.timedelta(days=self.periods_between)
        return [starting_time + (i * delta) for i in range(0, amount_of_dates)]

    @log_call
    def _get_dates_weekly_(self, starting_time, amount_of_dates):
        result = []
        delta = datetime.timedelta(weeks=self.periods_between)
        moments = [datetime.timedelta(days=moment) for moment in self.moments]
        for i in range(0, amount_of_dates // len(moments)):
            result.extend([starting_time + moment for moment in moments])
            starting_time += delta
        result.extend([starting_time + moments[i] for i in range(0, amount_of_dates % len(moments))])
        return result

    @log_call
    def _get_dates_monthly(self, starting_time, amount_of_dates):
        result = []
        dates = [starting_time + datetime.timedelta(days=moment) for moment in self.moments]
        for i in range(0, amount_of_dates // len(dates)):
            for date in dates:
                result.append(self._find_day_(date.year + (date.month - 1 + i * self.periods_between) // 12,
                                              (date.month - 1 + i * self.periods_between) % 12 + 1, date.day))

        i = amount_of_dates // len(dates)
        for j in range(0, amount_of_dates % len(dates)):
            result.append(self._find_day_(dates[j].year + (dates[j].month - 1 + i * self.periods_between) // 12,
                                          (dates[j].month - 1 + i * self.periods_between) % 12 + 1, dates[j].day))
        return result

    @log_call
    def _get_dates_annually(self, starting_time, amount_of_dates):
        result = []
        dates = [starting_time + datetime.timedelta(days=moment) for moment in self.moments]
        for i in range(0, amount_of_dates // len(dates)):
            for date in dates:
                result.append(self._find_day_(date.year + i * self.periods_between, date.month, date.day))

        i = amount_of_dates // len(dates)
        for j in range(0, amount_of_dates % len(dates)):
                result.append(self._find_day_(dates[j].year + i * self.periods_between, dates[j].month, dates[j].day))
        return result

    @log_call
    def get_all_dates(self, starting_time=None, amount_of_dates=None):
        """
            Returns amount_of_dates dates of this (self) schedule, if zero moment is starting_time
            :return: [datetime.date]
        """
        if starting_time is None:
            starting_time = self.first_creation_time
        if amount_of_dates is None:
            amount_of_dates = self.total_amount_of_tasks

        if self.period_type == PeriodType.DAILY:
            return self._get_dates_daily_(starting_time, amount_of_dates)
        if self.period_type == PeriodType.WEEKLY:
            return self._get_dates_weekly_(starting_time, amount_of_dates)
        if self.period_type == PeriodType.MONTHLY:
            return self._get_dates_monthly(starting_time, amount_of_dates)
        if self.period_type == PeriodType.ANNUALLY:
            return self._get_dates_annually(starting_time, amount_of_dates)

    @staticmethod
    @log_call
    def find_by_id(id):
        return db_interface.ScheduleDBActions.find_by_id(id)


class SchedulePlanLink:

    def __init__(self, first_creation_time, total_amount_of_tasks, issue_id, schedule_id, id=None):
        self.first_creation_time = first_creation_time
        self.issue_id = issue_id
        self.schedule_id = schedule_id
        self.total_amount_of_tasks = total_amount_of_tasks
        if id is not None:
            self.id = id

    @log_call
    def save(self):
        db_interface.ScheduleDBActions.save_link(self)

    @staticmethod
    @log_call
    def attach_plan_to_schedule(plan, schedule, first_creation_time, total_amount_of_tasks):
        inst = SchedulePlanLink(first_creation_time, total_amount_of_tasks, plan.id, schedule.id)
        inst.id = db_interface.ScheduleDBActions.add_link(inst).id
        return inst

    @log_call
    def delete(self):
        db_interface.ScheduleDBActions.delete_link(self)