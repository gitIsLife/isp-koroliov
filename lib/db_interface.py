"""
    File contains classes that work with orm and used by lib.
    Notice that every lib method may use database. So explicit managing of connection is the best practice.
    To manage database connection use: connect(), close(), is_closed().
"""

import datetime
from lib import (
    issue,
    user,
    schedule,
    plan,
    peewee_models,
    permissions,
    issue_base
)


def connect(reuse_if_open=False):
    return peewee_models.database_proxy.obj.connect(reuse_if_open=reuse_if_open)


def close():
    return peewee_models.database_proxy.obj.close()


def is_closed():
    return peewee_models.database_proxy.obj.is_closed()


def change_database(database_location):
    peewee_models.use_database(database_location)


class CommonDBActions:

    @staticmethod
    def create(model, obj):
        return model.create(**vars(obj))

    @staticmethod
    def save(model, obj):
        model = model.get_by_id(obj.id)
        for attr in vars(obj):
            model.__setattr__(attr, obj.__getattribute__(attr))
        model.save()

    @staticmethod
    def delete(model, obj):
        model.delete_by_id(obj.id)


class PlanDBActions:

    @staticmethod
    def _init(plan_model):
        delta = None
        if plan_model.deadline_since_start is not None:
            delta = plan_model.deadline_since_start - datetime.datetime(year=1980, month=1, day=1)
        return plan.Plan(plan_model.title, plan_model.description, plan_model.owner_id.id,
                               delta, plan_model.deadline_action_id, plan_model.priority, plan_model.id)

    @staticmethod
    def find_by_owner(user_id):
        issues = list(peewee_models.PlanModel.select().where(peewee_models.PlanModel.owner_id == user_id))
        return [PlanDBActions._init(model) for model in issues]

    @staticmethod
    def find_by_id(id):
        model = peewee_models.PlanModel.get_or_none(peewee_models.PlanModel.id == id)
        if model is not None:
            return PlanDBActions._init(model)
        else:
            return None

    @staticmethod
    def create(plan):
        deadline = plan.deadline_since_start
        if deadline is not None:
            plan.deadline_since_start = datetime.datetime(year=1980, month=1, day=1) + deadline
        created = CommonDBActions.create(peewee_models.PlanModel, plan)
        created.deadline_since_start = deadline
        return created

    @staticmethod
    def save(plan):
        deadline = plan.deadline_since_start
        if deadline is not None:
            plan.deadline_since_start = datetime.datetime(year=1980, month=1, day=1) + deadline
        CommonDBActions.save(peewee_models.PlanModel, plan)
        plan.deadline_since_start = deadline

    @staticmethod
    def delete(plan):
        CommonDBActions.delete(peewee_models.PlanModel, plan)

    @staticmethod
    def find_by_ids(ids):
        if not ids:
            return []
        sel = list(peewee_models.PlanModel.select().where(peewee_models.PlanModel.id << ids))
        return [PlanDBActions._init(plan) for plan in sel]


class IssueDBActions:

    @staticmethod
    def get_status(id):
        model = peewee_models.IssueModel.get_by_id(id)
        return model.status.id

    @staticmethod
    def set_status(id, stat):
        model = peewee_models.IssueModel.get_by_id(id)
        model.status = stat
        model.save()

    @staticmethod
    def save_tdp(issue):
        model = peewee_models.IssueModel.get_by_id(issue.id)
        model.title = issue.title
        model.description = issue.description
        model.priority = issue.priority
        model.plan_id = None
        model.save()

    @staticmethod
    def save_priority(issue):
        model = peewee_models.IssueModel.get_by_id(issue.id)
        model.priority = issue.priority
        model.save()

    @staticmethod
    def save_deadline_action(issue):
        model = peewee_models.IssueModel.get_by_id(issue.id)
        model.deadline_action = issue.deadline_action
        model.save()

    @staticmethod
    def create(issue_to_create):
        kwargs = {attr: issue_to_create._raw_getattribute_(attr) for attr in vars(issue_to_create)}
        kwargs['status'] = issue.TaskStatus.NOT_STARTED
        return peewee_models.IssueModel.create(**kwargs)

    @staticmethod
    def save(issue):
        model = peewee_models.IssueModel.get_by_id(issue.id)
        for attr in vars(issue):
            model.__setattr__(attr, issue._raw_getattribute_(attr))
        model.save()

    @staticmethod
    def _init(task):
        return issue.Issue(task.title, task.description, task.priority, task.deadline, task.deadline_action_id, task.creation_time,
                           task.owner_id_id, task.id, task.plan_id_id)

    @staticmethod
    def find_by_owner(owner):
        issues = list(peewee_models.IssueModel.select().where(peewee_models.IssueModel.owner_id == owner))
        return [IssueDBActions._init(task) for task in issues]

    @staticmethod
    def find_by_plan(plan_id):
        issues = list(peewee_models.IssueModel.select().where(peewee_models.IssueModel.plan_id == plan_id))
        return [IssueDBActions._init(task) for task in issues]

    @staticmethod
    def find_by_id(id):
        issue = peewee_models.IssueModel.get_or_none(peewee_models.IssueModel.id == id)
        if issue is None:
            return None
        return IssueDBActions._init(issue)

    @staticmethod
    def find_by_ids(ids):
        if not ids:
            return []
        select = list(peewee_models.IssueModel.select().where(peewee_models.IssueModel.id << ids))
        return [IssueDBActions._init(issue) for issue in select]

    @staticmethod
    def delete(id):
        peewee_models.IssueModel.get_by_id(id).delete_instance()


class UserDBActions:

    @staticmethod
    def find_by_login(login):
        user_model = peewee_models.UserModel.get_or_none(peewee_models.UserModel.login == login)
        if user_model is not None:
            return user.User(user_model.login, user_model.password, user_model.id,
                             user_model.notifications_priority_bound, user_model.notifications_settings)
        else:
            return None

    @staticmethod
    def find_by_id(id):
        user_model = peewee_models.UserModel.get_by_id(id)
        if user_model is not None:
            return user.User(user_model.login, user_model.password, user_model.id,
                             user_model.notifications_priority_bound, user_model.notifications_settings)
        else:
            return None

    @staticmethod
    def create(user):
        return peewee_models.UserModel.create(login=user.login, password=user.password,
                                              notifications_priority_bound=51, notifications_settings=0)

    @staticmethod
    def save(user):
        model = peewee_models.UserModel.get_by_id(user.id)
        model.login = user.login
        model.password = user.password
        model.notifications_priority_bound = user.notifications_bound
        model.notifications_settings = user.notifications_settings
        model.save()

    @staticmethod
    def delete(user):
        model = peewee_models.UserModel.get_by_id(user.id)
        model.delete_instance()

    @staticmethod
    def get_all_logins():
        return [user.login for user in list(peewee_models.UserModel.select())]


class IssueBindingsCommonDBActions:

    @classmethod
    def model(cls):
        pass

    @classmethod
    def ctor(cls, first_id, second_id, link):
        pass

    @classmethod
    def _select_by_link_and_construct(cls, select, link):
        if link is not None:
            select = select.where(cls.model().link == link)
        select = list(select)
        return [cls.ctor(b.first_id.id, b.second_id.id, b.link.id) for b in select]


    @classmethod
    def delete(cls, issue_bindings):
        first = cls.model().select().where(cls.model().first_id == issue_bindings.first_id)
        second = first.where(cls.model().second_id == issue_bindings.second_id)
        second.where(cls.model().link == issue_bindings.link).get().delete_instance()

    @classmethod
    def find_by_id(cls, issue_id, link=None):
        select = cls.model().select().where((cls.model().first_id == issue_id) | (cls.model().second_id == issue_id))
        return cls._select_by_link_and_construct(select, link)

    @classmethod
    def find_by_id_first(cls, issue_id, link=None):
        select = cls.model().select().where(cls.model().first_id == issue_id)
        return cls._select_by_link_and_construct(select, link)

    @classmethod
    def find_by_id_second(cls, issue_id, link=None):
        select = cls.model().select().where(cls.model().second_id == issue_id)
        return cls._select_by_link_and_construct(select, link)

    @classmethod
    def find_by_ids(cls, first_id, second_id):
        select = list(cls.model().select().where((cls.model().first_id == first_id) & (cls.model().second_id == second_id)))
        if not select:
            return None
        else:
            return cls.ctor(select[0].first_id.id, select[0].second_id.id, select[0].link.id)

    @classmethod
    def create(cls, issue_bindings):
        CommonDBActions.create(cls.model(), issue_bindings)


class IssueBindingsDBActions(IssueBindingsCommonDBActions):

    @classmethod
    def model(cls):
        return peewee_models.IssueBindingsModel

    @classmethod
    def ctor(cls, first_id, second_id, link):
        return issue.IssueBindings(first_id, second_id, link)

    @classmethod
    def create_many(cls, bindings_list):
        if not bindings_list:
            return
        model_class = peewee_models.IssueBindingsModel
        fields = [model_class.first_id, model_class.second_id, model_class.link]
        model_class.insert_many(bindings_list, fields).execute()


class PlanBindingsDBActions(IssueBindingsCommonDBActions):

    @classmethod
    def model(cls):
        return peewee_models.PlanBindingsModel

    @classmethod
    def ctor(cls, first_id, second_id, link):
        return plan.PlanBindings(first_id, second_id, link)


class ScheduleDBActions:

    @staticmethod
    def create(schedule):
        return CommonDBActions.create(peewee_models.ScheduleModel, schedule)

    @staticmethod
    def add_moment(schedule, moment):
        return peewee_models.ScheduleTimeMomentModel.create(schedule=schedule.id, moment=moment)

    @staticmethod
    def remove_moment(schedule, moment):
        model_cls = peewee_models.ScheduleTimeMomentModel
        obj = model_cls.get_or_none((model_cls.moment == moment) & (model_cls.schedule.id == schedule.id))
        if obj is not None:
            obj.delete_instance()

    @staticmethod
    def get_moments(schedule):
        model_cls = peewee_models.ScheduleTimeMomentModel
        select = list(model_cls.select().where(model_cls.schedule_id == schedule.id))
        return [model.moment for model in select]

    @staticmethod
    def delete(schedule):
        model_cls = peewee_models.ScheduleTimeMomentModel
        model_cls.delete().where(model_cls.schedule.id == schedule.id).execute()
        peewee_models.ScheduleModel.get_by_id(schedule.id).delete_instance()

    @staticmethod
    def add_link(link):
        return CommonDBActions.create(peewee_models.SchedulePlanLinkModel, link)

    @staticmethod
    def save_link(link):
        CommonDBActions.save(peewee_models.SchedulePlanLinkModel, link)

    @staticmethod
    def delete_link(link):
        CommonDBActions.delete(peewee_models.SchedulePlanLinkModel, link)

    @staticmethod
    def find_link(schedule_id, plan_id):
        model = peewee_models.SchedulePlanLinkModel
        select = list(model.select().where((model.schedule_id.id == schedule_id) & (model.issue_id.id == plan_id)))
        return [schedule.SchedulePlanLink(link.first_creation_time, link.total_amount_of_tasks,
                                          plan_id, schedule_id) for link in select]

    @staticmethod
    def find_by_plan_id(id):
        model = peewee_models.SchedulePlanLinkModel
        select = model.select().where(model.issue_id_id == id)
        schedules = []
        for model in select:
            schedule_model = model.schedule_id
            schedules.append(schedule.Schedule(schedule_model.period_type_id, schedule_model.periods_between,
                                               schedule_model.id, model.first_creation_time, model.total_amount_of_tasks))
        return schedules

    @staticmethod
    def find_by_id(id):
        model = peewee_models.ScheduleModel
        schedule_model = model.get_or_none(model.id == id)
        if schedule_model is None:
            return None
        return schedule.Schedule(schedule_model.period_type_id, schedule_model.periods_between, schedule_model.id)


class IssuePermissionCommonDBActions:

    @classmethod
    def model(cls):
        pass

    @classmethod
    def ctor(cls, issue_id, user_id, permission, login=None):
        pass

    @classmethod
    def issue_actions(cls):
        pass

    @classmethod
    def get_user_permissions(cls, user, permissions_lower_bound=issue_base.PermissionType.NO):
        select = cls.model().select().where((cls.model().user_id == user.id) & (cls.model().permission_id >= permissions_lower_bound))
        return [permission.issue_id for permission in select]

    @classmethod
    def delete_user_permissions(cls, user):
        cls.model().delete().where(cls.model().user_id == user.id).execute()

    @classmethod
    def get_user_issues(cls, user, permissions_lower_bound=issue_base.PermissionType.NO):
        model_cls = cls.model()
        select = model_cls.select().where((model_cls.user_id == user.id) & (model_cls.permission_id >= permissions_lower_bound))
        ids = [permission.issue_id for permission in select]
        result = cls.issue_actions().find_by_ids(ids)
        result.extend(cls.issue_actions().find_by_owner(user.id))
        return result

    @classmethod
    def set_permissions(cls, issue, user, permission):
        model_cls = cls.model()
        model = model_cls.get_or_none((model_cls.issue_id == issue.id) & (model_cls.user_id == user.id))
        if model is None:
            if permission != issue_base.PermissionType.NO:
                model_cls.create(issue=issue.id, user=user.id, permission=permission)
        else:
            if permission != issue_base.PermissionType.NO:
                model.permission = permission
                model.save()
            else:
                model.delete_instance()

    @classmethod
    def find_by_issue_id(cls, id):
        select = cls.model().select().where(cls.model().issue_id == id)
        return [cls.ctor(perm.issue_id, perm.user_id, perm.permission_id, perm.user.login) for perm in select]

    @classmethod
    def create_many(cls, permissions_list):
        if not permissions_list:
            return
        fields = [cls.model().issue, cls.model().user, cls.model().permission]
        cls.model().insert_many(permissions_list, fields).execute()

    @classmethod
    def delete(cls, permission):
        model = cls.model().get_or_none((cls.model().issue_id == permission.issue_id) & (cls.model().user_id == permission.user_id))
        if model is not None:
            model.delete_instance()


class IssuePermissionsDBActions(IssuePermissionCommonDBActions):

    @classmethod
    def model(cls):
        return peewee_models.IssuePermissionsModel

    @classmethod
    def ctor(cls, issue_id, user_id, permission, login=None):
        instance = permissions.IssuePermissions(issue_id, user_id, permission)
        if login is not None:
            instance.user_login = login
        return instance

    @classmethod
    def issue_actions(cls):
        return IssueDBActions


class PlanPermissionsDBActions(IssuePermissionCommonDBActions):

    @classmethod
    def model(cls):
        return peewee_models.PlanPermissionsModel

    @classmethod
    def ctor(cls, issue_id, user_id, permission, login=None):
        instance = permissions.PlanPermissions(issue_id, user_id, permission)
        if login is not None:
            instance.user_login = login
        return instance

    @classmethod
    def issue_actions(cls):
        return PlanDBActions


class TagCommonDBActions:

    @classmethod
    def model(cls):
        pass

    @classmethod
    def issue_interface(cls):
        pass

    @classmethod
    def add_tag(cls, issue, tag, user):
        tag, created = peewee_models.TagModel.get_or_create(tag=tag)
        cls.model().get_or_create(user=user.id, tag=tag.id, issue=issue.id)

    @classmethod
    def remove_tag(cls, issue, tag, user):
        tag, created = peewee_models.TagModel.get_or_create(tag=tag)
        link = cls.model().get_or_none((cls.model().issue_id == issue.id) & (cls.model().user_id == user.id)
                                       & (cls.model().tag_id == tag.id))
        if link is not None:
            link.delete_instance()

    @classmethod
    def get_tags(cls, issue, user):
        select = cls.model().select().where((cls.model().issue_id == issue.id) & (cls.model().user_id == user.id))
        return [model.tag.tag for model in select]

    @classmethod
    def get_tag_user_ids(cls, issue):
        select = cls.model().select().where(cls.model().issue_id == issue.id)
        return [(model.tag_id, model.user_id) for model in select]

    @classmethod
    def create_many(cls, data):  # data = [(issue_id, tag_id, user_id)]
        if not data:
            return
        fields = [cls.model().issue, cls.model().tag, cls.model().user]
        cls.model().insert_many(data, fields).execute()

    @classmethod
    def delete_all_tags(cls, issue):
        cls.model().delete().where(cls.model().issue_id == issue.id).execute()


class TagIssueDBActions(TagCommonDBActions):

    @classmethod
    def model(cls):
        return peewee_models.TagIssueLinkModel

    @classmethod
    def issue_interface(cls):
        return IssueDBActions


class TagPlanDBActions(TagCommonDBActions):

    @classmethod
    def model(cls):
        return peewee_models.TagPlanLinkModel

    @classmethod
    def issue_interface(cls):
        return PlanDBActions
