import unittest
from lib.tests import (
    issue_test,
    permissions_test,
    schedule_test,
    timing_test,
    user_test
)


def suite():
    suite = unittest.TestSuite()
    for test_case in [issue_test.PlanOperations, issue_test.IssueBindings, issue_test.IssueStatuses,
                      permissions_test.PermissionsTest, schedule_test.ScheduledIssueGraphs, timing_test.TimingTest,
                      user_test.IssueCreation, user_test.UserCreationDeletion]:
        suite.addTest(unittest.makeSuite(test_case))
    return suite
