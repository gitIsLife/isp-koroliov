import unittest
from datetime import datetime
from lib import user
from lib.issue import TaskStatus
from lib.issue_base import PermissionType


class PermissionsTest(unittest.TestCase):

    def setUp(self):
        u1 = user.User.log_in('permissions1', 'permissions1')
        u2 = user.User.log_in('permissions2', 'permissions2')
        u3 = user.User.log_in('permissions3', 'permissions3')
        for u in [u1, u2, u3]:
            if u is not None:
                u.delete()
        user.User.create('permissions1', 'permissions1')
        user.User.create('permissions2', 'permissions2')
        user.User.create('permissions3', 'permissions3')
        self.u1 = user.User.log_in('permissions1', 'permissions1')
        self.u2 = user.User.log_in('permissions2', 'permissions2')
        self.u3 = user.User.log_in('permissions3', 'permissions3')
        self.t1 = self.u1.new_issue('t1', 't1', 1, None, None)
        self.t2 = self.u2.new_issue('t2', 't2', 2, None, None)

    def tearDown(self):
        self.u1.delete()
        self.u3.delete()
        self.u2.delete()

    def test_a_a_no_status(self):
        self.assertTrue(self.t2.get_status(self.u2) == TaskStatus.NOT_STARTED)
        with self.assertRaises(PermissionError):
            self.t2.get_status(self.u1)
        with self.assertRaises(PermissionError):
            self.t2.save(self.u1)
        with self.assertRaises(PermissionError):
            self.t2.set_permission_to(self.u1, PermissionType.ADMIN, self.u1)

    def test_a_b_no_hierarchy(self):
        for u in [self.u1, self.u2]:
            with self.assertRaises(PermissionError):
                self.t2.add_alternative(self.t1, u)
            with self.assertRaises(PermissionError):
                self.t2.set_parent_issue(self.t1, u)
            with self.assertRaises(PermissionError):
                self.t2.add_blocking_issue(self.t1, u)
            with self.assertRaises(PermissionError):
                self.t1.add_mutually_exclusive(self.t2, u)

    def test_b_a_visible_status(self):
        self.t1.set_permission_to(self.u2, PermissionType.SPECTATOR, self.u1)
        self.assertTrue(self.t1.get_status(self.u1) == self.t1.get_status(self.u2))
        with self.assertRaises(PermissionError):
            self.t1.set_status(TaskStatus.INFORMATION_REQUIRED, self.u2)
        with self.assertRaises(PermissionError):
            self.t2.set_permission_to(self.u1, PermissionType.MANAGER, self.u1)

    def test_b_b_visible_blocking(self):
        self.t1.set_permission_to(self.u2, PermissionType.SPECTATOR, self.u1)
        self.t2.add_blocking_issue(self.t1, self.u2)
        with self.assertRaises(PermissionError):
            self.t2.remove_blocking_issue(self.t1, self.u1)
        self.t2.remove_blocking_issue(self.t1, self.u2)

    def test_b_c_visible_alternative(self):
        self.t1.set_permission_to(self.u2, PermissionType.SPECTATOR, self.u1)
        self.t2.add_alternative(self.t1, self.u2)
        with self.assertRaises(PermissionError):
            self.t2.remove_alternative(self.t1, self.u1)
            self.t2.remove_alternative(self.t1, self.u2)
        with self.assertRaises(PermissionError):
            self.t1.save(self.u2)

    def test_b_d_visible_other(self):
        self.t1.set_permission_to(self.u2, PermissionType.SPECTATOR, self.u1)
        for u in [self.u1, self.u2]:
            with self.assertRaises(PermissionError):
                self.t2.set_parent_issue(self.t1, u)
            with self.assertRaises(PermissionError):
                self.t1.set_parent_issue(self.t2, u)
            with self.assertRaises(PermissionError):
                self.t2.add_mutually_exclusive(self.t1, u)

    def test_c_a_participation(self):
        self.t1.set_permission_to(self.u2, PermissionType.PARTICIPANT, self.u1)
        with self.assertRaises(PermissionError):
            self.t2.set_permission_to(self.u1, PermissionType.PARTICIPANT, self.u1)
        self.assertTrue(self.t1.get_status(self.u1) == self.t1.get_status(self.u2))
        with self.assertRaises(PermissionError):
            self.t1.save(self.u2)
        self.t1.set_status(TaskStatus.COMPLETED, self.u2)

    def test_d_a_managed_save(self):
        self.t1.set_permission_to(self.u2, PermissionType.MANAGER, self.u1)
        self.t1.save(self.u2)
        with self.assertRaises(PermissionError):
            self.t2.set_permission_to(self.u1, PermissionType.PARTICIPANT, self.u1)
        self.assertTrue(self.t1.get_status(self.u1) == self.t1.get_status(self.u2))

    def test_d_b_managed_hierarchy(self):
        self.t1.set_permission_to(self.u2, PermissionType.MANAGER, self.u1)
        self.t1.add_alternative(self.t2, self.u2)
        self.t1.remove_alternative(self.t2, self.u2)
        self.t1.add_mutually_exclusive(self.t2, self.u2)
        self.t2.remove_mutually_exclusive(self.t1, self.u2)
        self.t1.add_blocking_issue(self.t2, self.u2)
        self.t1.remove_blocking_issue(self.t2, self.u2)
        self.t1.set_parent_issue(self.t2, self.u2)
        self.t1.set_parent_issue(None, self.u2)
        self.t2.set_parent_issue(self.t1, self.u2)

    def test_e_a_admin_permissions(self):
        self.t1.set_permission_to(self.u2, PermissionType.ADMIN, self.u1)
        self.t1.save(self.u2)
        for per in [p for p in PermissionType if p < PermissionType.ADMIN]:
            with self.assertRaises(PermissionError):
                self.t1.set_permission_to(self.u2, per, self.u3)
        for per in [p for p in PermissionType if p < PermissionType.ADMIN]:
            self.t1.set_permission_to(self.u3, per, self.u2)

    def test_e_b_admin_vs_admin(self):
        for u in [self.u2, self.u3]:
            self.t1.set_permission_to(u, PermissionType.ADMIN, self.u1)
        for per in [p for p in PermissionType if p < PermissionType.ADMIN]:
            with self.assertRaises(PermissionError):
                self.t1.set_permission_to(self.u2, per, self.u3)

    def test_f_a_plan_single(self):
        d1 = self.u1.new_plan('d1', 'd1', 21, None, None)
        d1.set_permission_to(self.u3, PermissionType.MANAGER, self.u1)
        with self.assertRaises(PermissionError):
            d1.set_permission_to(self.u2, PermissionType.SPECTATOR, self.u3)
        with self.assertRaises(PermissionError):
            d1.set_permission_to(self.u2, PermissionType.SPECTATOR, self.u2)
        d1._create_bindings_graph_(datetime.now())
        t1 = d1.issues[0]
        self.assertTrue(t1.is_managed_by(self.u3))
        self.assertFalse(t1.is_spectated_by(self.u2))
        self.assertTrue(t1.owner.id == self.u1.id)

    def test_f_b_plans_linked(self):
        d1 = self.u1.new_plan('d1', 'd1', 21, None, None)
        d2 = self.u2.new_plan('d2', 'd2', 22, None, None)
        d1.set_permission_to(self.u3, PermissionType.MANAGER, self.u1)
        d2.set_permission_to(self.u3, PermissionType.MANAGER, self.u2)
        d2.add_alternative(d1, self.u3)
        d2._create_bindings_graph_(datetime.now())
        self.assertTrue(d1.issues[0].alternatives[0].id == d2.issues[0].id)
        self.assertTrue(d1.issues[0].is_managed_by(self.u3))
        self.assertFalse(d1.issues[0].is_spectated_by(self.u2))
        self.assertTrue(d1.issues[0].owner.id == self.u1.id)
        self.assertTrue(d2.issues[0].is_managed_by(self.u3))
        self.assertTrue(d2.issues[0].owner.id == self.u2.id)
        self.assertFalse(d2.issues[0].is_spectated_by(self.u1))
