import unittest
import lib.user

User = lib.user.User


class UserCreationDeletion(unittest.TestCase):

    def test_a_create(self):
        if User.log_in('test', 'test') is not None:
            User.log_in('test', 'test').delete()
        if User.log_in('test1', 'test1') is not None:
            User.log_in('test1', 'test1').delete()
        User.create('test', 'test')
        User.create('test1', 'test1')

    def test_b_log_on(self):
        self.assertIsNone(User.log_in('not existing', 'tested'))
        self.assertIsNotNone(User.log_in('test', 'test'))
        self.assertIsNone(User.log_in('test', 'wrong'))
        self.assertIsNotNone(User.log_in('test1', 'test1'))

    def test_c_change_password(self):
        user = User.log_in('test', 'test')
        user.password = 'new'
        user.save()

    def test_d_log_on_new_password(self):
        self.assertIsNotNone(User.log_in('test', 'new'))
        self.assertIsNone(User.log_in('test', 'test'))

    def test_e_change_login(self):
        user = User.log_in('test', 'new')
        with self.assertRaises(ValueError):
            user.login = 'test1'
            user.save()
        user.login = 'test2'
        user.save()

    def test_f_delete(self):
        User.log_in('test2', 'new').delete()
        User.log_in('test1', 'test1').delete()


class IssueCreation(unittest.TestCase):

    def test_a_create_user(self):
        User.create('testing issues', 'testing issues')

    def test_b_create_issue(self):
        user = User.log_in('testing issues', 'testing issues')
        with self.assertRaises(ValueError):
            user.new_issue(None, 'asd', 50, None, None)
        self.assertIsNotNone(user.new_issue('title', 'descr', 50, None, None))
        self.assertIsNotNone(user.new_issue('title', 'descr', 50, None, None))

    def test_c_delete_issue(self):
        user = User.log_in('testing issues', 'testing issues')
        user.owned_issues[0].delete()

    def test_d_delete_user(self):
        User.log_in('testing issues', 'testing issues').delete()
