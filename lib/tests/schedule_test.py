import datetime
import unittest

from lib import user


class ScheduledIssueGraphs(unittest.TestCase):

    def setUp(self):
        u = user.User.log_in('testing graphs', 'testing graphs')
        if u is not None:
            u.delete()
        user.User.create('testing graphs', 'testing graphs')
        self.user = user.User.log_in('testing graphs', 'testing graphs')
        self.t1 = self.user.new_plan('t1', 't1', 1, None, None)
        self.t2 = self.user.new_plan('t2', 't2', 2, None, None)
        self.t3 = self.user.new_plan('t3', 't3', 3, None, None)
        self.t4 = self.user.new_plan('t4', 't4', 4, None, None)
        self.t5 = self.user.new_plan('t5', 't5', 5, None, None)
        self.t6 = self.user.new_plan('t6', 't6', 6, None, None)

    def tearDown(self):
        self.user.delete()

    def test_a_a_same_graph_not_connected(self):
        for t in [self.t1, self.t2, self.t3, self.t4, self.t5]:
            for q in [self.t1, self.t2, self.t3, self.t4, self.t5]:
                self.assertTrue(t._is_in_same_graph_with_(q))
                self.assertTrue(q._is_in_same_graph_with_(t))

    def test_a_b_same_graph_connected(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.t3.set_parent_issue(self.t4, self.user)
        self.assertTrue(self.t1._is_in_same_graph_with_(self.t2))
        self.assertTrue(self.t3._is_in_same_graph_with_(self.t4))
        for t in [self.t1, self.t2]:
            for q in [self.t3, self.t4]:
                self.assertFalse(t._is_in_same_graph_with_(q))
                self.assertFalse(q._is_in_same_graph_with_(t))

    def test_a_c_graph_exceptions(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.t3.set_parent_issue(self.t4, self.user)
        for t in [self.t1, self.t2]:
            for q in [self.t3, self.t4]:
                with self.assertRaises(ValueError):
                    t.add_alternative(q, self.user)
                with self.assertRaises(ValueError):
                    q.set_parent_issue(t, self.user)
                with self.assertRaises(ValueError):
                    t.add_blocking_issue(q, self.user)
                with self.assertRaises(ValueError):
                    q.add_mutually_exclusive(t, self.user)

    def test_a_d_alternative_graph(self):
        for t in [self.t2, self.t3, self.t4]:
            t.add_alternative(self.t1, self.user)
        for t in [self.t1, self.t2, self.t3, self.t4]:
            for q in [self.t1, self.t2, self.t3, self.t4]:
                self.assertTrue(t._is_in_same_graph_with_(q))
                self.assertTrue(q._is_in_same_graph_with_(t))

    def test_b_a_creating_alternative_issues(self):
        for t in [self.t1, self.t2, self.t3, self.t4]:
            t.add_alternative(self.t1, self.user)
        self.t1._create_bindings_graph_(datetime.datetime.now())
        for t in [self.t2, self.t3, self.t4]:
            self.assertTrue(t.issues[0].alternatives[0].id == self.t1.issues[0].id)
        self.assertTrue(len(self.t1.alternatives) == 3)

    def test_b_b_creating_parenting_issues(self):
        for t in [self.t2, self.t3]:
            t.set_parent_issue(self.t1, self.user)
        self.t4.set_parent_issue(self.t3, self.user)
        self.t1._create_bindings_graph_(datetime.datetime.now())
        self.assertTrue(self.t1.issues[0].parent_issue is None)
        self.assertTrue(self.t2.issues[0].parent_issue.id == self.t1.issues[0].id)
        self.assertTrue(self.t3.issues[0].parent_issue.id == self.t1.issues[0].id)
        self.assertTrue(self.t4.issues[0].parent_issue.id == self.t3.issues[0].id)

    def test_b_c_creating_mutually_exclusives(self):
        for t in [self.t1, self.t2, self.t5, self.t6]:
            t.set_parent_issue(self.t3, self.user)
        self.t3.add_mutually_exclusive(self.t4, self.user)
        self.t1._create_bindings_graph_(datetime.datetime.now())
        self.assertTrue(self.t3.issues[0].mutually_exclusives[0].id == self.t4.issues[0].id)
        self.assertTrue(self.t4.issues[0].mutually_exclusives[0].id == self.t3.issues[0].id)
        for q in [t.issues[0] for t in [self.t1, self.t2, self.t5, self.t6]]:
            self.assertFalse(q.mutually_exclusives)

    def test_b_d_creating_blocking_issues(self):
        for t in [self.t1, self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t5, self.user)
        self.t6.add_blocking_issue(self.t5, self.user)
        self.t1._create_bindings_graph_(datetime.datetime.now())
        self.assertTrue(self.t6.issues[0].blocked_by[0].id == self.t5.issues[0].id)
        self.assertTrue(self.t5.issues[0].blocks_following[0].id == self.t6.issues[0].id)
        for t in [q.issues[0] for q in [self.t1, self.t2, self.t3, self.t4]]:
            self.assertFalse(t.blocked_by)
            self.assertFalse(t.blocks_following)

    def test_c_a_creating_tags(self):
        self.t2.set_parent_issue(self.t1, self.user)
        self.t3.set_parent_issue(self.t1, self.user)
        self.t2.add_tag('tag2', self.user)
        self.t2.add_tag('tag22', self.user)
        self.t1.add_tag('tag1', self.user)
        self.t1._create_bindings_graph_(datetime.datetime.now())
        for t in ['tag2', 'tag22']:
            self.assertTrue(t in self.t2.issues[0].tags(self.user))
        self.assertTrue(len(self.t2.issues[0].tags(self.user)) == 2)
        self.assertTrue(self.t1.issues[0].tags(self.user)[0] == 'tag1')
        self.assertFalse(self.t3.issues[0].tags(self.user))
