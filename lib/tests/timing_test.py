import datetime
import time
import unittest
from lib.issue import TaskStatus, DeadlineActions
from lib import user


class TimingTest(unittest.TestCase):

    def setUp(self):
        if user.User.log_in('timing test', 'timing test') is not None:
            user.User.log_in('timing test', 'timing test').delete()
        user.User.create('timing test', 'timing test')
        self.user = user.User.log_in('timing test', 'timing test')
        self.t1 = self.user.new_issue('t1', 't1', 1, None, None)
        self.t2 = self.user.new_issue('t2', 't2', 2, None, None)
        self.t3 = self.user.new_issue('t3', 't3', 3, None, None)

    def tearDown(self):
        self.user.delete()

    def test_a_a_set_failed(self):
        d = datetime.datetime.now() + datetime.timedelta(seconds=1)
        for t in [self.t1, self.t2, self.t3]:
            t.deadline_action = DeadlineActions.SET_AS_FAILED
            t.deadline = d
            t.save(self.user)
        time.sleep(2)
        self.user.check_deadlines_and_plans()
        for t in [self.t1, self.t2, self.t3]:
            self.assertTrue(t.get_status(self.user) == TaskStatus.FAILED)

    def test_a_b_set_critical(self):
        d = datetime.datetime.now() + datetime.timedelta(seconds=1)
        for t in [self.t1, self.t2, self.t3]:
            t.set_status(TaskStatus.INFORMATION_REQUIRED, self.user)
            t.deadline_action = DeadlineActions.SET_AS_CRITICAL
            t.deadline = d
            t.save(self.user)
        time.sleep(2)
        self.user.check_deadlines_and_plans()
        for t in self.user.owned_issues:
            self.assertTrue(t.priority == 100)
            self.assertTrue(t.get_status(self.user) == TaskStatus.INFORMATION_REQUIRED)

    def test_a_c_nothing(self):
        d = datetime.datetime.now() + datetime.timedelta(seconds=1)
        for t in [self.t1, self.t2, self.t3]:
            t.set_status(TaskStatus.ON_THE_GO, self.user)
            t.deadline_action = DeadlineActions.NOTHING
            t.priority = 30
            t.deadline = d
            t.save(self.user)
        time.sleep(2)
        self.user.check_deadlines_and_plans()
        for t in [self.t1, self.t2, self.t3]:
            self.assertTrue(t.priority == 30)
            self.assertTrue(t.get_status(self.user) == TaskStatus.ON_THE_GO)

    def test_b_a_deadline_hierarchy(self):
        self.t3.set_parent_issue(self.t2, self.user)
        self.t2.set_parent_issue(self.t1, self.user)
        self.t3.deadline_action = DeadlineActions.SET_AS_CRITICAL
        self.t2.deadline_action = DeadlineActions.SET_AS_FAILED
        self.t1.deadline_action = DeadlineActions.NOTHING
        for t in [self.t1, self.t2, self.t3]:
            t.deadline = datetime.datetime.now() + datetime.timedelta(seconds=1)
            t.save(self.user)
        time.sleep(2)
        self.user.check_deadlines_and_plans()
        t3 = self.user.owned_issues[2]
        self.assertTrue(t3.priority == 100)
        self.assertTrue(t3.get_status(self.user) == TaskStatus.NOT_STARTED)
        for t in [self.t1, self.t2]:
            self.assertTrue(t.get_status(self.user) == TaskStatus.FAILED)

    def test_c_a_plans_deadlines(self):
        d1 = self.user.new_plan('d1', 'd1', 11, datetime.timedelta(seconds=1), DeadlineActions.SET_AS_FAILED)
        d2 = self.user.new_plan('d2', 'd2', 12, datetime.timedelta(seconds=1), DeadlineActions.NOTHING)
        d3 = self.user.new_plan('d3', 'd3', 13, datetime.timedelta(seconds=1), DeadlineActions.SET_AS_CRITICAL)
        for d in [d2, d3]:
            d1.add_alternative(d, self.user)
        d1._create_bindings_graph_(datetime.datetime.now())
        time.sleep(2)
        self.user.check_deadlines_and_plans()
        self.assertTrue(d1.issues[0].get_status(self.user) == TaskStatus.FAILED)
        for t in [d2.issues[0], d3.issues[0]]:
            self.assertTrue(t.get_status(self.user) == TaskStatus.NOT_STARTED)
        self.assertTrue(d1.issues[0].priority == 11)
        self.assertTrue(d2.issues[0].priority == 12)
        self.assertTrue(d3.issues[0].priority == 100)
