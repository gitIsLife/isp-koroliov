import unittest

from lib.user import User
from lib.issue import TaskStatus


class PlanOperations(unittest.TestCase):

    def setUp(self):
        if User.log_in('testing issues', 'testing issues') is not None:
            User.log_in('testing issues', 'testing issues').delete()
        User.create('testing issues', 'testing issues')
        self.user = User.log_in('testing issues', 'testing issues')

    def tearDown(self):
        self.user.delete()

    def test_a_a_create_plan(self):
        self.user.new_plan('test', 'test', 50, None, None)
        with self.assertRaises(ValueError):
            self.user.new_plan(None, 'test', 50, None, None)

    def test_a_b_updating_plan(self):
        d = self.user.new_plan('test', 'test', 50, None, None)
        d.title = 'abcdef'
        d.save(self.user)
        with self.assertRaises(ValueError):
            d.title = None
            d.save(self.user)
        d.title = 'modif'
        d.save(self.user)
        d.delete()

    def test_b_a_issue_from_plan(self):
        d = self.user.new_plan('test', 'dtest', 50, None, None)
        for i in range(0, 4):
            self.assertIsNotNone(d.create_issue())
        d.delete()
        for i in self.user.owned_issues:
            self.assertIsNone(i.plan)
            self.assertEqual(i.title, 'test')
            self.assertEqual(i.description, 'dtest')

    def test_c_a_modifying_plan(self):
        d = self.user.new_plan('test', 'test', 50, None, None)
        self.assertIsNotNone(d.create_issue())
        d.title = 'new'
        d.save(self.user)
        self.assertEqual('new', self.user.owned_issues[0].title)
        d.delete()
        self.assertEqual('new', self.user.owned_issues[0].title)


class IssueBindings(unittest.TestCase):

    def setUp(self):
        if User.log_in('testing bindings', 'testing bindings') is None:
            User.create('testing bindings', 'testing bindings')
        self.user = User.log_in('testing bindings', 'testing bindings')
        self.t1 = self.user.new_issue('1', '1', 1, None, None)
        self.t2 = self.user.new_issue('2', '2', 2, None, None)
        self.t3 = self.user.new_issue('3', '3', 3, None, None)
        self.t4 = self.user.new_issue('4', '4', 4, None, None)

    def tearDown(self):
        self.user.delete()

    def test_a_a_siblings_with_no_parent(self):
        self.assertTrue(self.t1._is_sibling_(self.t2))
        self.assertTrue(self.t1._is_sibling_(self.t3))
        self.assertTrue(self.t3._is_sibling_(self.t2))

    def test_a_b_siblibgs(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.assertFalse(self.t1._is_sibling_(self.t2))
        self.assertFalse(self.t1._is_sibling_(self.t3))
        self.assertTrue(self.t3._is_sibling_(self.t2))

        self.t3.set_parent_issue(self.t2, self.user)
        self.assertTrue(self.t3._is_sibling_(self.t1))
        self.assertTrue(self.t1._is_sibling_(self.t3))

        self.t3.set_parent_issue(self.t1, self.user)
        self.assertFalse(self.t1._is_sibling_(self.t2))
        self.assertFalse(self.t1._is_sibling_(self.t3))
        self.assertFalse(self.t3._is_sibling_(self.t2))

    def test_a_c_none_sibling(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.t3.set_parent_issue(self.t1, self.user)
        self.t3.set_parent_issue(None, self.user)
        self.assertFalse(self.t1._is_sibling_(self.t2))
        self.assertFalse(self.t1._is_sibling_(self.t3))
        self.assertTrue(self.t3._is_sibling_(self.t2))

    def test_a_d_parentage_exceptions(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.t3.set_parent_issue(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t2.set_parent_issue(self.t3, self.user)
        self.t3.set_parent_issue(None, self.user)
        with self.assertRaises(ValueError):
            self.t2.set_parent_issue(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t2.set_parent_issue(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t1.set_parent_issue(self.t1, self.user)

    def test_b_a_add_blocking(self):
        self.t1.add_blocking_issue(self.t2, self.user)
        self.t2.add_blocking_issue(self.t3, self.user)
        with self.assertRaises(ValueError):
            self.t2.add_blocking_issue(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t2.add_blocking_issue(self.t2, self.user)
        self.assertTrue(self.t1.blocked_by[0].id == self.t2.id)
        self.assertFalse(self.t1.blocks_following)
        self.assertTrue(self.t2.blocked_by[0].id == self.t3.id)
        self.assertTrue(self.t2.blocks_following[0].id == self.t1.id)
        self.assertFalse(self.t3.blocked_by)
        self.assertTrue(self.t3.blocks_following[0].id == self.t2.id)

    def test_b_b_remove_blocking(self):
        self.t1.add_blocking_issue(self.t2, self.user)
        self.t2.add_blocking_issue(self.t3, self.user)
        self.t1.remove_blocking_issue(self.t2, self.user)
        self.assertFalse(self.t1.blocked_by)
        self.assertFalse(self.t1.blocks_following)
        self.assertTrue(self.t2.blocked_by[0].id == self.t3.id)
        self.assertFalse(self.t2.blocks_following)
        self.assertFalse(self.t3.blocked_by)
        self.assertTrue(self.t3.blocks_following[0].id == self.t2.id)

    def test_b_c_blocking_exceptions(self):
        self.t2.add_blocking_issue(self.t3, self.user)
        with self.assertRaises(ValueError):
            self.t2.set_parent_issue(self.t3, self.user)
        self.t1.set_parent_issue(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t1.remove_blocking_issue(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t1.add_blocking_issue(self.t2, self.user)

    def test_c_a_add_remove_alternatives(self):
        self.t1.add_alternative(self.t2, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.assertTrue(self.t1.alternatives[0].id == self.t2.id)
        self.assertTrue(self.t3.alternatives[0].id == self.t2.id)
        self.assertTrue(len(self.t2.alternatives) == 2)

        self.t2.remove_alternative(self.t3, self.user)
        self.assertTrue(self.t1.alternatives[0].id == self.t2.id)
        self.assertFalse(self.t3.alternatives)
        self.assertTrue(self.t2.alternatives[0].id == self.t1.id)

    def test_c_b_alternative_exceptions(self):
        self.t1.add_alternative(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t1.set_parent_issue(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t1.set_parent_issue(self.t3, self.user)
        with self.assertRaises(ValueError):
            self.t2.set_parent_issue(self.t3, self.user)
        self.t3.set_parent_issue(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t2.remove_alternative(self.t3, self.user)
        self.t2.remove_alternative(self.t1, self.user)
        for t in [self.t1, self.t2, self.t3]:
            self.assertFalse(t.alternatives)
        with self.assertRaises(ValueError):
            self.t3.add_alternative(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t1.add_alternative(self.t3, self.user)

    def test_d_a_add_mutually_exclusive(self):
        self.t1.add_mutually_exclusive(self.t2, self.user)
        self.assertTrue(self.t1.mutually_exclusives[0].id == self.t2.id)
        self.assertTrue(self.t2.mutually_exclusives[0].id == self.t1.id)
        self.assertFalse(self.t3.mutually_exclusives)

        self.t2.add_mutually_exclusive(self.t3, self.user)
        self.assertTrue(self.t1.mutually_exclusives[0].id == self.t2.id)
        self.assertTrue(self.t3.mutually_exclusives[0].id == self.t2.id)
        self.assertTrue(len(self.t2.mutually_exclusives) == 2)

    def test_d_b_remove_mutually_exclusive(self):
        self.t2.add_mutually_exclusive(self.t3, self.user)
        self.t1.add_mutually_exclusive(self.t2, self.user)
        self.t2.remove_mutually_exclusive(self.t1, self.user)
        self.assertTrue(self.t2.mutually_exclusives[0].id == self.t3.id)
        self.assertTrue(self.t3.mutually_exclusives[0].id == self.t2.id)
        self.assertFalse(self.t1.mutually_exclusives)

    def test_d_c_exclusives_exceptions(self):
        self.t3.set_parent_issue(self.t4, self.user)
        self.t4.set_parent_issue(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t1.add_mutually_exclusive(self.t3, self.user)
        with self.assertRaises(ValueError):
            self.t1.add_mutually_exclusive(self.t4, self.user)
        with self.assertRaises(ValueError):
            self.t3.add_mutually_exclusive(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t3.add_mutually_exclusive(self.t4, self.user)


class IssueStatuses(unittest.TestCase):

    def setUp(self):
        if User.log_in('testing statuses', 'testing statuses') is not None:
            User.log_in('testing statuses', 'testing statuses').delete()
        User.create('testing statuses', 'testing statuses')
        self.user = User.log_in('testing statuses', 'testing statuses')
        self.t1 = self.user.new_issue('1', '1', 1, None, None)
        self.t2 = self.user.new_issue('2', '2', 2, None, None)
        self.t3 = self.user.new_issue('3', '3', 3, None, None)
        self.t4 = self.user.new_issue('4', '4', 4, None, None)
        self.t5 = self.user.new_issue('5', '5', 5, None, None)
        self.t6 = self.user.new_issue('6', '6', 6, None, None)

    def tearDown(self):
        self.user.delete()

    def test_a_a_uncompleted_children(self):
        for t in [self.t1, self.t3, self.t4]:
            t.set_parent_issue(self.t2, self.user)
        with self.assertRaises(ValueError):
            self.t2._set_status_(TaskStatus.COMPLETED)
        self.t1._set_status_(TaskStatus.ON_THE_GO)
        self.t2._set_status_(TaskStatus.ON_THE_GO)
        self.t2._set_status_(TaskStatus.INFORMATION_REQUIRED)
        self.t2._set_status_(TaskStatus.NOT_STARTED)
        with self.assertRaises(ValueError):
            self.t2._set_status_(TaskStatus.COMPLETED)

    def test_a_b_failed_child(self):
        for t in [self.t1, self.t3, self.t4]:
            t.set_parent_issue(self.t2, self.user)
        self.t1._set_status_(TaskStatus.FAILED)
        self.assertTrue(self.t2._get_status_() == TaskStatus.FAILED)
        for status in [TaskStatus.ON_THE_GO, TaskStatus.INFORMATION_REQUIRED,
                       TaskStatus.NOT_STARTED]:
            with self.assertRaises(ValueError):
                self.t2._set_status_(status)

    def test_a_c_complete_parentage(self):
        for t in [self.t1, self.t3, self.t4]:
            t.set_parent_issue(self.t2, self.user)
            t._set_status_(TaskStatus.COMPLETED)
        self.t2._set_status_(TaskStatus.COMPLETED)

    def test_a_d_fail_child(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.t1._set_status_(TaskStatus.FAILED)
        self.assertTrue(self.t2._get_status_() == TaskStatus.FAILED)

    def test_b_a_ancestor_exceptions(self):
        for t in [self.t2, self.t5, self.t6]:
            t.set_parent_issue(self.t1, self.user)
        self.t3.set_parent_issue(self.t2, self.user)
        self.t4.set_parent_issue(self.t3, self.user)
        for t in [self.t1, self.t2, self.t3, self.t4, self.t5, self.t6]:
            with self.assertRaises(ValueError):
                self.t1.set_parent_issue(t, self.user)

    def test_b_b_is_ancestor_of(self):
        for t in [self.t2, self.t5, self.t6]:
            t.set_parent_issue(self.t1, self.user)
        self.t3.set_parent_issue(self.t2, self.user)
        self.t4.set_parent_issue(self.t3, self.user)
        for t in [self.t1, self.t2, self.t3]:
            self.assertTrue(t._is_ancestor_of_(self.t4))
        self.t3.set_parent_issue(self.t1, self.user)
        for t in [self.t2, self.t5, self.t6]:
            self.assertTrue(t._is_sibling_(self.t3))
        self.t2.set_parent_issue(self.t4, self.user)
        self.t6.set_parent_issue(self.t2, self.user)
        self.t5.set_parent_issue(self.t6, self.user)

    def test_c_a_alternative_fail(self):
        for t in [self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t1, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.t3.add_alternative(self.t4, self.user)
        self.t2._set_status_(TaskStatus.FAILED)
        self.assertFalse(self.t1._get_status_() == TaskStatus.FAILED)
        self.t4._set_status_(TaskStatus.FAILED)
        self.assertTrue(self.t1._get_status_() == TaskStatus.NOT_STARTED)
        self.t4._set_status_(TaskStatus.INFORMATION_REQUIRED)
        self.assertTrue(self.t1._get_status_() == TaskStatus.NOT_STARTED)
        self.t3._set_status_(TaskStatus.FAILED)
        self.assertTrue(self.t1._get_status_() == TaskStatus.FAILED)
        with self.assertRaises(ValueError):
            self.t1._set_status_(TaskStatus.ON_THE_GO)

    def test_c_b_alternatives_complete(self):
        for t in [self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t1, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.t3.add_alternative(self.t4, self.user)
        self.t3._set_status_(TaskStatus.COMPLETED)
        self.t1._set_status_(TaskStatus.COMPLETED)

    def test_c_c_alternatives_exceptions(self):
        for t in [self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t1, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.t3.add_alternative(self.t4, self.user)
        self.t5.add_alternative(self.t6, self.user)
        with self.assertRaises(ValueError):
            self.t5.set_parent_issue(self.t3, self.user)
        with self.assertRaises(ValueError):
            self.t6.set_parent_issue(self.t2, self.user)

    def test_c_d_uncompleted_alternatives(self):
        for t in [self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t1, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.t3.add_alternative(self.t4, self.user)
        for t in [self.t3, self.t1, self.t5]:
            t._set_status_(TaskStatus.COMPLETED)
        self.t5.set_parent_issue(self.t3, self.user)
        self.t5._set_status_(TaskStatus.INFORMATION_REQUIRED)
        self.assertTrue(self.t3._get_status_() == TaskStatus.ON_THE_GO)
        self.assertTrue(self.t1._get_status_() == TaskStatus.ON_THE_GO)

    def test_c_d_remove_alternative(self):
        for t in [self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t1, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.t3.add_alternative(self.t4, self.user)
        for t in [self.t3, self.t1]:
            t._set_status_(TaskStatus.COMPLETED)
        self.t4._set_status_(TaskStatus.NOT_STARTED)
        self.t2._set_status_(TaskStatus.FAILED)
        self.t3.remove_alternative(self.t4, self.user)
        self.assertTrue(self.t1._get_status_() == TaskStatus.ON_THE_GO)
        self.t3.remove_alternative(self.t2, self.user)
        self.assertTrue(self.t1._get_status_() == TaskStatus.FAILED)

    def test_d_a_alternatives(self):
        for t in [self.t2, self.t3, self.t4]:
            t.set_parent_issue(self.t1, self.user)
        self.t2.add_alternative(self.t3, self.user)
        self.t3.add_alternative(self.t4, self.user)
        self.assertTrue(self.t2.alternatives[0].id == self.t3.id)
        self.assertTrue(self.t4.alternatives[0].id == self.t3.id)
        self.assertTrue(len(self.t3.alternatives) == 2)
        self.t2.add_alternative(self.t4, self.user)
        for t in [self.t2, self.t3, self.t4]:
            self.assertTrue(len(t.alternatives) == 2)
            with self.assertRaises(ValueError):
                t.remove_alternative(self.t1, self.user)

    def test_e_a_blocking_add_exceptions(self):
        self.t2._set_status_(TaskStatus.FAILED)
        with self.assertRaises(ValueError):
            self.t1.add_blocking_issue(self.t2, self.user)
        self.t2._set_status_(TaskStatus.NOT_STARTED)
        self.t1._set_status_(TaskStatus.COMPLETED)
        with self.assertRaises(ValueError):
            self.t1.add_blocking_issue(self.t2, self.user)

    def test_e_b_blocking_uncompleted_exceptions(self):
        self.t2._set_status_(TaskStatus.NOT_STARTED)
        self.t1._set_status_(TaskStatus.ON_THE_GO)
        self.t1.add_blocking_issue(self.t2, self.user)
        self.t2.add_blocking_issue(self.t3, self.user)
        with self.assertRaises(ValueError):
            self.t2.add_blocking_issue(self.t1, self.user)
        with self.assertRaises(ValueError):
            self.t2._set_status_(TaskStatus.COMPLETED)
        with self.assertRaises(ValueError):
            self.t1._set_status_(TaskStatus.COMPLETED)

    def test_e_c_failed_blocking(self):
        self.t2._set_status_(TaskStatus.NOT_STARTED)
        self.t1._set_status_(TaskStatus.ON_THE_GO)
        self.t1.add_blocking_issue(self.t2, self.user)
        self.t2.add_blocking_issue(self.t3, self.user)
        self.t3._set_status_(TaskStatus.FAILED)
        self.assertTrue(self.t1._get_status_() == TaskStatus.FAILED)
        self.assertTrue(self.t2._get_status_() == TaskStatus.FAILED)
        with self.assertRaises(ValueError):
            self.t2._set_status_(TaskStatus.ON_THE_GO)
        with self.assertRaises(ValueError):
            self.t1._set_status_(TaskStatus.INFORMATION_REQUIRED)

    def test_e_d_blocking_uncompleted_trig(self):
        self.t1.add_blocking_issue(self.t2, self.user)
        self.t2.add_blocking_issue(self.t3, self.user)
        for t in [self.t3, self.t2, self.t1]:
            t._set_status_(TaskStatus.COMPLETED)
        self.t2._set_status_(TaskStatus.INFORMATION_REQUIRED)
        self.assertTrue(self.t1._get_status_() == TaskStatus.ON_THE_GO)
        self.assertTrue(self.t3._get_status_() == TaskStatus.COMPLETED)

    def test_f_a_blocking_hierarchy_exceptions(self):
        self.t1.set_parent_issue(self.t2, self.user)
        self.t3.set_parent_issue(self.t1, self.user)
        for t in [self.t1, self.t2, self.t3]:
            for q in [self.t1, self.t2, self.t3]:
                with self.assertRaises(ValueError):
                    t.add_blocking_issue(q, self.user)
                with self.assertRaises(ValueError):
                    q.add_blocking_issue(t, self.user)

    def test_f_b_blocking_properties(self):
        self.t1.add_blocking_issue(self.t2, self.user)
        self.t1.add_blocking_issue(self.t3, self.user)
        self.assertTrue(self.t2.blocks_following[0].id == self.t1.id)
        self.assertTrue(self.t3.blocks_following[0].id == self.t1.id)
        self.assertFalse(self.t1.blocks_following)
        self.assertFalse(self.t2.blocked_by)
        self.assertFalse(self.t3.blocked_by)
        self.assertTrue(len(self.t1.blocked_by) == 2)

    def test_g_a_exclusive_single(self):
        for t in [self.t2, self.t3]:
            self.t1.add_mutually_exclusive(t, self.user)
        self.t2._set_status_(TaskStatus.COMPLETED)
        self.assertTrue(self.t1._get_status_() == TaskStatus.FAILED)
        with self.assertRaises(ValueError):
            self.t1._set_status_(TaskStatus.COMPLETED)

    def test_g_b_exclusive_multi(self):
        for t in [self.t2, self.t3]:
            self.t1.add_mutually_exclusive(t, self.user)
        self.t2._set_status_(TaskStatus.INFORMATION_REQUIRED)
        self.t1._set_status_(TaskStatus.COMPLETED)
        for t in [self.t2, self.t3]:
            self.assertTrue(t._get_status_() == TaskStatus.FAILED)
            with self.assertRaises(ValueError):
                t._set_status_(TaskStatus.NOT_STARTED)
        self.t1.remove_mutually_exclusive(self.t2, self.user)
        self.t2._set_status_(TaskStatus.COMPLETED)

    def test_h_a_exclusive_hierarchy_exceptions(self):
        self.t4.set_parent_issue(self.t2, self.user)
        self.t3.set_parent_issue(self.t2, self.user)
        self.t2.set_parent_issue(self.t1, self.user)
        self.t4.add_mutually_exclusive(self.t3, self.user)
        for t in [self.t1, self.t2, self.t3]:
            for q in [self.t1, self.t2, self.t3]:
                with self.assertRaises(ValueError):
                    t.add_mutually_exclusive(q, self.user)
                with self.assertRaises(ValueError):
                    q.add_mutually_exclusive(t, self.user)
        with self.assertRaises(ValueError):
            self.t3.set_parent_issue(self.t4, self.user)

    def test_h_b_exclusives(self):
        self.t4.add_mutually_exclusive(self.t3, self.user)
        self.t2.add_mutually_exclusive(self.t3, self.user)
        self.assertTrue(self.t2.mutually_exclusives[0].id == self.t3.id)
        self.assertTrue(self.t4.mutually_exclusives[0].id == self.t3.id)
        self.assertTrue(len(self.t3.mutually_exclusives) == 2)

    def test_i_a_plan_parentage(self):
        t1 = self.user.new_plan('t1', 't1', 1, None, None)
        t2 = self.user.new_plan('t2', 't2', 2, None, None)
        t3 = self.user.new_plan('t3', 't3', 3, None, None)
        t4 = self.user.new_plan('t4', 't4', 4, None, None)
        t1.set_parent_issue(t2, self.user)
        t2.set_parent_issue(t3, self.user)
        with self.assertRaises(ValueError):
            t3.set_parent_issue(t1, self.user)
        with self.assertRaises(ValueError):
            t1.add_alternative(t4, self.user)
        t4.set_parent_issue(t2, self.user)

    def test_i_b_plan_alternative(self):
        t1 = self.user.new_plan('t1', 't1', 1, None, None)
        t2 = self.user.new_plan('t2', 't2', 2, None, None)
        t3 = self.user.new_plan('t3', 't3', 3, None, None)
        t4 = self.user.new_plan('t4', 't4', 4, None, None)
        t1.set_parent_issue(t2, self.user)
        t2.set_parent_issue(t3, self.user)
        t4.set_parent_issue(t2, self.user)
        t4.add_alternative(t1, self.user)
        with self.assertRaises(ValueError):
            t4.set_parent_issue(t3, self.user)
        t4.set_parent_issue(t2, self.user)
        t4.remove_alternative(t1, self.user)
        t4.set_parent_issue(t3, self.user)

    def test_i_c_plan_siblings(self):
        t1 = self.user.new_plan('t1', 't1', 1, None, None)
        t2 = self.user.new_plan('t2', 't2', 2, None, None)
        t3 = self.user.new_plan('t3', 't3', 3, None, None)
        t4 = self.user.new_plan('t4', 't4', 4, None, None)
        t1.set_parent_issue(t2, self.user)
        t2.set_parent_issue(t3, self.user)
        t4.set_parent_issue(t2, self.user)
        t4.set_parent_issue(t3, self.user)
        self.assertTrue(t4._is_sibling_(t2))
        self.assertFalse(t4._is_sibling_(t1))

    def test_i_d_plan_mutually_exclusive(self):
        t1 = self.user.new_plan('t1', 't1', 1, None, None)
        t2 = self.user.new_plan('t2', 't2', 2, None, None)
        t3 = self.user.new_plan('t3', 't3', 3, None, None)
        t4 = self.user.new_plan('t4', 't4', 4, None, None)
        t1.set_parent_issue(t2, self.user)
        t2.set_parent_issue(t3, self.user)
        t4.set_parent_issue(t3, self.user)
        t4.add_mutually_exclusive(t1, self.user)
        t4.set_parent_issue(None, self.user)
        with self.assertRaises(ValueError):
            t3.set_parent_issue(t4, self.user)
