
"""
    This module has two decorators.
    log_call prints all arguments and value returned by method, which it is applied to
    log_class applies log_call to every method of class, including @staticmethod and @classmethod
"""

import logging
from functools import wraps


LIB_NAME = 'TaskTracker'


def log_call(method):
    @wraps(method)
    def with_log(*args, **kwargs):
        logger = get_logger()
        logger.info('IN ' + method.__name__ + ': ')
        if args:
            logger.debug(args)
        if kwargs:
            logger.debug(kwargs)
        try:
            result = method(*args, **kwargs)
        except Exception as e:
            if not hasattr(e, 'printed_traceback'):
                e.printed_traceback = True
                logger.error('------------------------------------------------------------------------')
                logger.error(str(e))
                logger.error('Traceback (most recent call first):')
            logger.error('IN ' + method.__name__ + ': ')
            if args:
                logger.error(args)
            if kwargs:
                logger.error(kwargs)
            raise
        logger.debug(method.__name__ + ' RETURNS: ' + str(result))
        return result
    return with_log


def get_logger():
    return logging.getLogger(LIB_NAME)
