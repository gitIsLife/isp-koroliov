from enum import IntEnum
from lib.logging import log_call

NO_PERMISSION_ERR_MSG = "You haven't got enough permissions to do that"


class EnumHelper:
    @staticmethod
    def contains(enum, value):
        return value in list(map(int, enum))


class TaskLink(IntEnum):
    PARENT_CHILD = 2
    BLOCKED_BLOCKING = 3  # blocked cannot be completed before blocking, if blocking fails blocked fails too
    ALTERNATIVE = 4  # can be set up only between tasks-siblings; allows complete parent task with completed only one of alternative tasks
    MUTUALLY_EXCLUSIVE = 5  # when one task is completed, other automatically fails


class PermissionType(IntEnum):
    NO = 1
    SPECTATOR = 2
    CONSULTANT = 3
    PARTICIPANT = 4
    MANAGER = 5
    ADMIN = 6


class IssueBase:

    """
        This class includes some common parts of Issue and Plan.
        Do not use this class, because almost every method requires specific settings, which are defined in descendants.
    """

    @classmethod
    def _actions_(cls):
        pass

    @classmethod
    def _tag_actions_(cls):
        pass

    @classmethod
    def _bindings_actions_(cls):
        pass

    @classmethod
    def _bindings_class_(cls):
        pass

    @classmethod
    def _permissions_actions_(cls):
        pass

    @log_call
    def _links_(self, link=None):
        return self._bindings_actions_().find_by_id(self.id, link)

    @classmethod
    @log_call
    def get_ids_visible_to(cls, user):
        return cls._permissions_actions_().get_user_permissions(user)

    @classmethod
    @log_call
    def get_visible_to(cls, user):
        return cls._permissions_actions_().get_user_issues(user)

    @log_call
    def is_visible_to(self, user):
        task = self
        ids = self.get_ids_visible_to(user)
        while task is not None:
            if task.id in ids or task.owner_id == user.id:
                return True
            task = task.parent_issue
        return False

    @classmethod
    @log_call
    def get_ids_spectated_by(cls, user):
        return cls._permissions_actions_().get_user_permissions(user, PermissionType.SPECTATOR)

    @log_call
    def is_spectated_by(self, user):
        task = self
        ids = self.get_ids_spectated_by(user)
        while task is not None:
            if task.id in ids or task.owner_id == user.id:
                return True
            task = task.parent_issue
        return False

    @classmethod
    @log_call
    def get_ids_participating_to(cls, user):
        return cls._permissions_actions_().get_user_permissions(user, PermissionType.PARTICIPANT)

    @log_call
    def is_participated_by(self, user):
        task = self
        ids = self.get_ids_participating_to(user)
        while task is not None:
            if task.id in ids or task.owner_id == user.id:
                return True
            task = task.parent_issue
        return False

    def __repr__(self):
        string = self.__class__.__name__ + '<'
        for attr in vars(self):
            string += "'" + attr + "': " + str(self.__getattribute__(attr)) + ', '
        return string[:-2] + '>'

    @classmethod
    @log_call
    def get_ids_managed_by(cls, user):
        return cls._permissions_actions_().get_user_permissions(user, PermissionType.MANAGER)

    @log_call
    def is_managed_by(self, user):
        task = self
        ids = self.get_ids_managed_by(user)
        while task is not None:
            if task.id in ids or task.owner_id == user.id:
                return True
            task = task.parent_issue
        return False

    @classmethod
    @log_call
    def get_ids_administrated_by(cls, user):
        return cls._permissions_actions_().get_user_permissions(user, PermissionType.ADMIN)

    @log_call
    def is_administrated_by(self, user):
        task = self
        ids = self.get_ids_administrated_by(user)
        while task is not None:
            if task.id in ids or task.owner_id == user.id:
                return True
            task = task.parent_issue
        return False

    @log_call
    def is_owned_by(self, user):
        return self.owner_id == user.id

    @property
    @log_call
    def parent_issue(self):
        """
            Finds in database super-issue of conrete issue(self).
            :return: issue or None.
        """
        list = self._bindings_actions_().find_by_id_second(self.id, TaskLink.PARENT_CHILD)
        if not list:
            return None
        else:
            parent_id = (list[0]).first_id
            return self._actions_().find_by_id(parent_id)

    @log_call
    def _is_sibling_(self, other):
        if self.parent_issue is None:
            if other.parent_issue is None:
                return True
            else:
                return False
        else:
            if other.parent_issue is None:
                return False
            else:
                if other.parent_issue.id is self.parent_issue.id:
                    return True
                else:
                    return False

    @log_call
    def _is_ancestor_of_(self, other):
        ancestor = other.parent_issue
        while ancestor is not None:
            if ancestor.id == self.id:
                return True
            else:
                ancestor = ancestor.parent_issue
        return False

    @log_call
    def set_parent_issue(self, new_parent, user):
        """
            Saves to database new parent of concrete issue (self)
            :param user: User, who tries to perform this action (must have appropriate permissions)
        """
        if not self.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        old_parent = self._bindings_actions_().find_by_id_second(self.id, TaskLink.PARENT_CHILD)
        if self.parent_issue is not None:
            if not self.parent_issue.is_managed_by(user):
                raise PermissionError(NO_PERMISSION_ERR_MSG)

        if new_parent is not None:
            if not new_parent.is_managed_by(user):
                raise PermissionError(NO_PERMISSION_ERR_MSG)

            old_link = self._bindings_class_()._find_link(self.id, new_parent.id)
            if self.id is new_parent.id:
                raise ValueError('No parentage with self')

            if old_link is not None:
                if old_link.first_id == new_parent.id and old_link.link == TaskLink.PARENT_CHILD:
                    return
                raise ValueError('This issues have already been linked')

            if self._is_ancestor_of_(new_parent):
                raise ValueError('No parentage cycles allowed')

            for block in new_parent.blocked_by:
                if self._is_ancestor_of_(block):
                    raise ValueError('Child blocks new parent')

            for blocked in new_parent.blocks_following:
                if self._is_ancestor_of_(blocked):
                    raise ValueError('New parent blocks child')

            for exclusive in new_parent.mutually_exclusives:
                if self._is_ancestor_of_(exclusive):
                    raise ValueError('New parent is mutual exclusive with child')

        if self.alternatives:
            raise ValueError('Issue has alternative issues')

        if old_parent:
            old_parent[0].delete()
        if new_parent is not None:
            self._bindings_class_().create(new_parent, self, TaskLink.PARENT_CHILD)

    @property
    @log_call
    def children_issues(self):
        """
            Finds in database subissues of concrete issue (self).
            :return: [issue]
        """
        bindings = self._bindings_actions_().find_by_id_first(self.id, TaskLink.PARENT_CHILD)
        return [self._actions_().find_by_id(binding.second_id) for binding in bindings]

    @property
    @log_call
    def blocked_by(self):
        bindings = self._bindings_actions_().find_by_id_first(self.id, TaskLink.BLOCKED_BLOCKING)
        return [self._actions_().find_by_id(binding.second_id) for binding in bindings]

    @property
    @log_call
    def blocks_following(self):
        bindings = self._bindings_actions_().find_by_id_second(self.id, TaskLink.BLOCKED_BLOCKING)
        return [self._actions_().find_by_id(binding.first_id) for binding in bindings]

    @log_call
    def add_blocking_issue(self, blocking, user):
        if not self.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not blocking.is_spectated_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if self.id is blocking.id:
            raise ValueError('You cannot block yourselves')

        if blocking._is_ancestor_of_(self) or self._is_ancestor_of_(blocking):
            raise ValueError('You cannot block your kinsman')  # родственник

        old_link = self._bindings_class_()._find_link(self.id, blocking.id)
        if old_link is not None:
            if old_link.first_id == self.id and old_link.link == TaskLink.BLOCKED_BLOCKING:
                return
            raise ValueError('This issues have already been linked')

        self._bindings_class_().create(self, blocking, TaskLink.BLOCKED_BLOCKING)

    @log_call
    def remove_blocking_issue(self, blocking, user):
        if blocking is None or self.id is blocking.id:
            return

        if not self.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        old_link = self._bindings_class_()._find_link(self.id, blocking.id)
        if old_link is not None:
            if old_link.first_id == self.id and old_link.link == TaskLink.BLOCKED_BLOCKING:
                old_link.delete()
                return
            raise ValueError('This issues have another link type')

    @property
    @log_call
    def alternatives(self):
        bindings = self._bindings_actions_().find_by_id(self.id, TaskLink.ALTERNATIVE)
        result = []
        for binding in bindings:
            if binding.first_id == self.id:
                result.append(self._actions_().find_by_id(binding.second_id))
            else:
                result.append(self._actions_().find_by_id(binding.first_id))
        return result

    @log_call
    def add_alternative(self, alternative, user):
        if alternative is None or alternative.id == self.id:
            return

        parent = self.parent_issue
        if parent is not None:
            if not parent.is_managed_by(user):
                raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not alternative.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not self.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        old_link = self._bindings_class_()._find_link(self.id, alternative.id)
        if old_link is not None:
            if old_link.link == TaskLink.ALTERNATIVE:
                return
            raise ValueError('This issues have already been linked')

        if not self._is_sibling_(alternative):
            raise ValueError('Alternative link can be set up only between tasks-siblings')

        self._bindings_class_().create(self, alternative, TaskLink.ALTERNATIVE)

    def _update_parent_status_(self):
        pass

    @log_call
    def remove_alternative(self, alternative, user):
        if alternative is None or self.id is alternative.id:
            return

        parent = self.parent_issue
        if parent is not None:
            if not parent.is_managed_by(user):
                raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not alternative.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not self.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        old_link = self._bindings_class_()._find_link(self.id, alternative.id)
        if old_link is not None:
            if old_link.link == TaskLink.ALTERNATIVE:
                old_link.delete()
                self._update_parent_status_()
                return
            raise ValueError('This issues have another link type')

    @property
    @log_call
    def mutually_exclusives(self):
        bindings = self._bindings_actions_().find_by_id(self.id, TaskLink.MUTUALLY_EXCLUSIVE)
        result = []
        for binding in bindings:
            if binding.first_id == self.id:
                result.append(self._actions_().find_by_id(binding.second_id))
            else:
                result.append(self._actions_().find_by_id(binding.first_id))
        return result

    @log_call
    def add_mutually_exclusive(self, exclusive, user):
        if exclusive is None:
            return

        if not self.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not exclusive.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if exclusive.id == self.id:
            raise ValueError('You cannot be mutually exclusive with yourselves')

        if self._is_ancestor_of_(exclusive) or exclusive._is_ancestor_of_(self):
            raise ValueError('No exclusions with kinsman allowed')

        old_link = self._bindings_class_()._find_link(self.id, exclusive.id)
        if old_link is not None:
            if old_link.link == TaskLink.MUTUALLY_EXCLUSIVE:
                return
            raise ValueError('This issues have already been linked')

        self._bindings_class_().create(self, exclusive, TaskLink.MUTUALLY_EXCLUSIVE)

    @log_call
    def remove_mutually_exclusive(self, exclusive, user):
        if exclusive is None or self.id is exclusive.id:
            return

        if not self.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not exclusive.is_managed_by(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        old_link = self._bindings_class_()._find_link(self.id, exclusive.id)
        if old_link is not None:
            if old_link.link == TaskLink.MUTUALLY_EXCLUSIVE:
                old_link.delete()
                return
            raise ValueError('This issues have another link type')

    @log_call
    def set_permission_to(self, user, permission, user_who_grants_permission):
        if not self.is_administrated_by(user_who_grants_permission):
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if self.owner_id == user.id:
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if self.is_administrated_by(user) and not self.owner_id == user_who_grants_permission.id:
            raise PermissionError(NO_PERMISSION_ERR_MSG)

        if not EnumHelper.contains(PermissionType, permission):
            raise ValueError('Incorrect permission type')

        self._permissions_actions_().set_permissions(self, user, permission)

    @classmethod
    @log_call
    def find_by_id(cls, id):
        if id is None:
            return None
        return cls._actions_().find_by_id(id)

    @log_call
    def all_permissions(self):
        """
            Finds in database all permissions connected with this issue (self).
            :return: [permission]
        """
        return self._permissions_actions_().find_by_issue_id(self.id)

    @log_call
    def add_tag(self, tag, user):
        if not self.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)
        self._tag_actions_().add_tag(self, tag, user)

    @log_call
    def remove_tag(self, tag, user):
        if not self.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)
        self._tag_actions_().remove_tag(self, tag, user)

    @log_call
    def tags(self, user):
        if not self.is_visible_to(user):
            raise PermissionError(NO_PERMISSION_ERR_MSG)
        return self._tag_actions_().get_tags(self, user)

    @log_call
    def _get_tag_user_ids_(self):
        """
            :return: [(tag_id, user_id)]
        """
        return self._tag_actions_().get_tag_user_ids(self)

    @log_call
    def _clear_tags_(self):
        self._tag_actions_().delete_all_tags(self)

    @log_call
    def _get_graph_ids_and_representative(self):
        ids = set()
        add = {self.id}
        while add:
            ids = ids.union(add)
            new_add = set()
            for issue_id in add:
                bindings = self._bindings_actions_().find_by_id(issue_id)
                for binding in bindings:
                    if binding.first_id not in ids:
                        new_add.add(binding.first_id)
                    if binding.second_id not in ids:
                        new_add.add(binding.second_id)
            add = new_add
        return (ids, min(ids))


class IssueBindingsBase:

    """
        This class handles issue relationships. But to manage relationships you should call methods of Issue or Plan.
    """

    @classmethod
    def _actions_(cls):
        pass

    def __init__(self, first_id, second_id, link):
        self.first_id = first_id
        self.second_id = second_id
        self.link = link

    def _validate(self):
        if not EnumHelper.contains(TaskLink, self.link):
            raise ValueError("Link must be of type TaskLink(Enum)")

    def delete(self):
        self._actions_().delete(self)

    @classmethod
    @log_call
    def create(cls, first, second, link):
        inst = cls(first.id, second.id, link)
        inst._validate()
        cls._actions_().create(inst)

    @classmethod
    @log_call
    def _find_link(cls, first_id, second_id):
        link = cls._actions_().find_by_ids(first_id, second_id)
        if link is None:
            return cls._actions_().find_by_ids(second_id, first_id)
        else:
            return link