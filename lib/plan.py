import datetime
from lib import issue_base, permissions, db_interface, issue, schedule
from lib.logging import log_call

NOT_IN_GRAPH_ERR_MSG = 'Plans must belong to the same component'


class Plan(issue_base.IssueBase):

    """
        This class represents planned issue.
        Use Plan.create() to create plan instead of ctor.
    """

    @classmethod
    def _actions_(cls):
        return db_interface.PlanDBActions

    @classmethod
    def _tag_actions_(cls):
        return db_interface.TagPlanDBActions

    @classmethod
    def _bindings_actions_(cls):
        return db_interface.PlanBindingsDBActions

    @classmethod
    def _bindings_class_(cls):
        return PlanBindings

    @classmethod
    def _permissions_actions_(cls):
        return db_interface.PlanPermissionsDBActions

    @property
    @log_call
    def owner(self):
        """
            Selects from database User, whose id is owner_id of concrete Plan (self).
            :return: User
        """
        id = self.owner_id
        return db_interface.UserDBActions.find_by_id(id)

    def __init__(self, title, description, owner_id, deadline_since_start, deadline_action, priority=50, id=None):
        self.title = title
        self.description = description
        self.owner_id = owner_id
        self.priority = priority
        self.deadline_since_start = deadline_since_start
        self.deadline_action = deadline_action
        if id is not None:
            self.id = id

    @staticmethod
    @log_call
    def create(title, description, owner_id, deadline_since_start, deadline_action, priority=50):
        """
            Saves to database new object of Plan.
            Title, description and priority must be defined, otherwise exception will be thrown
            :return: Plan
        """
        inst = Plan(title, description, owner_id, deadline_since_start, deadline_action, priority)
        inst._validate()
        inst.id = db_interface.PlanDBActions.create(inst).id
        return inst

    def _validate(self):
        if self.title is None or self.description is None or self.priority is None or self.owner_id is None:
            raise ValueError(issue.NONE_FIELD_ERR_MSG)

        if self.deadline_action is not None and not issue_base.EnumHelper.contains(issue.DeadlineActions, self.deadline_action):
            return ValueError(issue.WRONG_DEADLINE_ACTION_ERR_MSG)

    @log_call
    def save(self, user):
        """
            Saves changes to this issue (self) to database.
            Raises exception if title or description or priority is None.
        """
        if not self.is_managed_by(user):
            raise PermissionError(issue_base.NO_PERMISSION_ERR_MSG)
        self._validate()
        db_interface.PlanDBActions.save(self)

    @property
    @log_call
    def issues(self):
        """
            Returns list of issues, created by this Plan (self).
        """
        return db_interface.IssueDBActions.find_by_plan(self.id)

    @log_call
    def delete(self):
        """
            Deletes concrete issue (self), all its links with other issues and all associated permissions.
        """
        for task in self.issues:
            task._plan_deleting_()

        for link in self._links_():
            link.delete()

        for permission in self._permissions_actions_().find_by_issue_id(self.id):
            permission.delete()

        self._tag_actions_().delete_all_tags(self)
        db_interface.PlanDBActions.delete(self)

    @log_call
    def create_issue(self, creation_time=None):
        """
            Creates Issue from concrete Plan and sets its deadline in appropriate way.
            :param creation_time: if None issue 'is created' now
            :return: Issue
        """
        self._validate()
        if creation_time is None:
            creation_time = datetime.datetime.now()

        if self.deadline_since_start is not None:
            return issue.Issue.create(title=None, description=None, priority=None,
                                      deadline=creation_time + self.deadline_since_start,
                                      deadline_action=self.deadline_action, owner_id=self.owner_id,
                                      plan_id=self.id, creation_time=creation_time)

        else:
            return issue.Issue.create(title=None, description=None, priority=None, deadline=None, deadline_action=None,
                                      owner_id=self.owner_id, plan_id=self.id, creation_time=creation_time)

    @log_call
    def _is_in_same_graph_with_(self, other):
        if other is None:
            return True
        ids1, repr1 = self._get_graph_ids_and_representative()
        ids2, repr2 = other._get_graph_ids_and_representative()
        if repr1 == repr2 or len(ids1) == 1 or len(ids2) == 1:
            return True
        else:
            return False

    @log_call
    def _create_bindings_graph_(self, creation_time):
        ids = set()
        add = {self.id}
        links = set()
        while add:
            ids = ids.union(add)
            new_add = set()
            for plan_id in add:
                bindings = db_interface.PlanBindingsDBActions.find_by_id(plan_id)
                for binding in bindings:
                    if binding.first_id not in ids:
                        new_add.add(binding.first_id)

                    if binding.second_id not in ids:
                        new_add.add(binding.second_id)

                    link = (binding.first_id, binding.second_id, binding.link)
                    if link not in links:
                        links.add(link)
            add = new_add

        mapping = dict()
        permissions_list = []
        tags_data = []
        for id in ids:
            plan = db_interface.PlanDBActions.find_by_id(id)
            new_issue = plan.create_issue(creation_time)

            for tagID_userID in plan._get_tag_user_ids_():
                tags_data.append((new_issue.id, tagID_userID[0], tagID_userID[1]))

            mapping[id] = new_issue.id
            old_links = db_interface.PlanPermissionsDBActions.find_by_issue_id(id)

            for link in old_links:
                permissions_list.append((new_issue.id, link.user_id, link.permission_type))

        bindings = [(mapping[link[0]], mapping[link[1]], link[2]) for link in links]
        issue.IssueBindings.create_many(bindings)
        permissions.IssuePermissions.create_many(permissions_list)
        issue.Issue._tag_actions_().create_many(tags_data)

    @property
    @log_call
    def schedules(self):
        """
            Returns list of schedules associated with this Plan (self).
        """
        return db_interface.ScheduleDBActions.find_by_plan_id(self.id)

    @log_call
    def add_schedule(self, schedule_to_add, first_creation_time, total_amount_of_tasks, user):
        ids, representative = self._get_graph_ids_and_representative()
        representative = db_interface.PlanDBActions.find_by_id(representative)
        if not representative.is_managed_by(user):
            raise PermissionError(issue_base.NO_PERMISSION_ERR_MSG)
        schedule.SchedulePlanLink.attach_plan_to_schedule(representative, schedule_to_add, first_creation_time, total_amount_of_tasks)

    @log_call
    def remove_schedule(self, schedule_to_remove):
        ids, representative = self._get_graph_ids_and_representative()
        link = db_interface.ScheduleDBActions.find_link(plan_id=representative, schedule_id=schedule_to_remove.id)
        if link is not None:
            link.delete()

    def set_parent_issue(self, new_parent, user):
        if not self._is_in_same_graph_with_(new_parent):
            raise ValueError(NOT_IN_GRAPH_ERR_MSG)
        super().set_parent_issue(new_parent, user)

    def add_alternative(self, alternative, user):
        if not self._is_in_same_graph_with_(alternative):
            raise ValueError(NOT_IN_GRAPH_ERR_MSG)
        super().add_alternative(alternative, user)

    def add_blocking_issue(self, blocking, user):
        if not self._is_in_same_graph_with_(blocking):
            raise ValueError(NOT_IN_GRAPH_ERR_MSG)
        super().add_blocking_issue(blocking, user)

    def add_mutually_exclusive(self, exclusive, user):
        if not self._is_in_same_graph_with_(exclusive):
            raise ValueError(NOT_IN_GRAPH_ERR_MSG)
        super().add_mutually_exclusive(exclusive, user)


class PlanBindings(issue_base.IssueBindingsBase):

    @classmethod
    def _actions_(cls):
        return db_interface.PlanBindingsDBActions

    def __init__(self, first_id, second_id, link):
        super().__init__(first_id, second_id, link)